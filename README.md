Wavelengths
   Version Number 1.6.1
   Version Date: 12/13/13

A computer game created entirely in Java.

-
-

![screenshot.png](https://bitbucket.org/repo/RnBk4K/images/3669868827-screenshot.png)


-
-



*by John Banya*


----------------------------------------------------------------------------------------------------------------------------
**What is Wavelengths?**

Wavelengths is a challenging side-scrolling computer game in which your goal is to solve puzzles by changing the colors of the world around you.

Each individual color has a unique ability to changes the basic properties/physics of the game upon contact with the player.

Levels can be solved by changing the color of tiles to create a method of reaching the (normally inaccessible) end.

-
-

**Controls**

'A'  or  Left Arrow Key: *Move Left*

'D'  or  Right Arrow Key: *Move Right*

Space Bar: *Jump*

Number Keys (1-4)  or  Mouse Scroll Wheel: *Change Selected Colors*

Mouse Left Click: *Change tile color*

Mouse Right Click: *Remove tile color*

'R': *Restart Level*

'esc': *Exit to Menu*

-
-

***Warning: Spoilers***

*Color Properties:*

*Blue: Increased jump (additional boost when connected to other blue tiles)*

*Orange: Increased speed (Resets when changing direction during contact)*

*Purple: Cushion (prevents fall death)*

*Light Blue: Remove solid property when in contact with player moving above a certain velocity (allows player to pass through normally-solid tiles)*



----------------------------------------------------------------------------------------------------------------------------
**How to view source code:**

Source-> src -> aer/res

aer: Contains packages of java source files

res: Contains resource files (image files)

*Executable .jar file available in Downloads*