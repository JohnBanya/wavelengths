package com.aer.Levels;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import com.aer.Entity.Entity;
import com.aer.Entity.Player;
import com.aer.Main.Game;
import com.aer.Main.Mouse;
import com.aer.Textures.AnimatedSprite;
import com.aer.Textures.Sprite;
import com.aer.Textures.SpriteSheet;
import com.aer.Tiles.Blank;
import com.aer.Tiles.Editable;
import com.aer.Tiles.LocalTeleportTile;
import com.aer.Tiles.SolidTile;
import com.aer.Tiles.TeleportTile;
import com.aer.Tiles.Tile;
import com.aer.Tiles.TileData;
import com.aer.Util.GameFont;

public class Level 
{
	protected int width_tiles, height_tiles;
	protected String path;
	
	protected int[] tileColors;
	protected Tile[] tiles;
	
	protected Player player;
	protected ArrayList<GameFont> gameFont = new ArrayList<GameFont>();
	protected ArrayList<Entity> entities = new ArrayList<Entity>();
	
	protected int totalColored = 0;
	protected final int MAX_COLORED;
	
	protected GameFont tilesColoredDisplay;
	protected int tilesColoredDisplayHex = 0xFFC4C4C4;
	
	
	public Level(String path, int maxColored)
	{
		this.path = path;
		MAX_COLORED = maxColored;
		tilesColoredDisplay = new GameFont("" + totalColored + "/" + MAX_COLORED, 10, 10, tilesColoredDisplayHex, 0, 0, -1, true);
		load(path);
	}
	
	protected void load(String path)
	{
		try
		{
			BufferedImage image = ImageIO.read(Game.class.getResource(path));
			width_tiles = image.getWidth();
			height_tiles = image.getHeight();
			tileColors = new int[width_tiles * height_tiles];
			tiles = new Tile[width_tiles * height_tiles];
			image.getRGB(0, 0, width_tiles, height_tiles, tileColors, 0, width_tiles);
			System.out.println("Level loaded successfully");
		}
		catch(Exception e)
		{
			System.out.println("Oops! Something went wrong!");
			e.printStackTrace();
		}
		

		loadTiles(tileColors);
	}
	
	public void render()
	{
		
		int xScroll = Game.screen.getXOffset();
		int yScroll = Game.screen.getYOffset();
		
		//corners
		int x0 = xScroll >> 4;
		int x1 = (xScroll + Game.screen.getWidth() + 16) >> 4;
		int y0 = yScroll >> 4;
		int y1 = (yScroll + Game.screen.getHeight() + 16) >> 4;
		
		for(int yt = y0; yt < y1; yt++)
		{
			for(int xt = x0; xt < x1; xt++)
			{
				if((yt < height_tiles) && (xt < width_tiles) && ((xt+yt*width_tiles) < tiles.length) && (xt >= 0) && (yt >= 0))
					tiles[xt+yt*width_tiles].render(xt, yt);
			}
		}
		
		for(int i = 0; i < entities.size(); i++)
		{
			entities.get(i).render();
		}
		
		for(int i = 0; i < gameFont.size(); i++)
		{
			gameFont.get(i).render();
		}
		
		if(player != null)
			player.render();
		
		Game.screen.renderIgnoreOffsetsRectangle(6, 7, tilesColoredDisplay.getWidth() + 10, 13, 0xFF383838);
		Game.screen.renderIgnoreOffsetsRectangle(7, 8, tilesColoredDisplay.getWidth() + 8, 11, 0xFF636363);
		Game.screen.renderIgnoreOffsetsRectangle(8, 9, tilesColoredDisplay.getWidth() + 6, 9, 0xFF404040);
		tilesColoredDisplay.render();
	}
	
	public void tick()
	{
		for(int i = 0; i < tiles.length; i++)
		{
			tiles[i].tick();
		}
		
		for(int i = 0; i < gameFont.size(); i++)
		{
			GameFont g = gameFont.get(i);
			g.tick();
			
			if(g.isRemoved())
				gameFont.remove(i);
			else
				gameFont.set(i, g);
		}
		
		for(int i = 0; i < entities.size(); i++)
		{
			Entity e = entities.get(i);
			e.tick();
			
			if(e.isRemoved())
				entities.remove(i);
			else
				entities.set(i, e);
		}
		
		
		if(player != null)
			player.tick();
		
		handleColorChange();
		
		tilesColoredDisplay = new GameFont("" + totalColored + "/" + MAX_COLORED, 10, 10, tilesColoredDisplayHex, 0, 0, -1, true);
		tilesColoredDisplay.tick();
	}
	
	protected void loadTiles(int[] tileColors)
	{
		for(int i = 0; i < tileColors.length; i++)
		{
			int col = tileColors[i];
			
			if(col == Tile.COL_EDITABLE)
				tiles[i] = new Editable(Sprite.editable_tile);
			else if(col == Tile.COL_SOLID)
				tiles[i] = new SolidTile(Sprite.solid_tile, 0xFFFFFFFF);
			else if(col == Tile.COL_SOLID_BLUE)
				tiles[i] = new SolidTile(Sprite.solid_tile, Player.PLAYER_COL_BLUE);
			else if(col == Tile.COL_SOLID_ORANGE)
				tiles[i] = new SolidTile(Sprite.solid_tile, Player.PLAYER_COL_ORANGE);
			else if(col == Tile.COL_SOLID_RED)
				tiles[i] = new SolidTile(Sprite.solid_tile, 0xFFFF0000);
			else if(col == Tile.COL_SOLID_PURPLE)
				tiles[i] = new SolidTile(Sprite.solid_tile, Player.PLAYER_COL_PURPLE);
			else if(col == Tile.COL_SOLID_GREEN)
				tiles[i] = new SolidTile(Sprite.solid_tile, Player.PLAYER_COL_GREEN);
			else if(col == Tile.COL_TELEPORT)
				tiles[i] = new TeleportTile(new AnimatedSprite(SpriteSheet.teleporter_animated, 0, 0, 16, 3));
			else if(col == Tile.COL_BLANK_EMPTY_WINDOW)
				tiles[i] = new Blank(Sprite.blank_tile_emptyWindow);
			else if(col == Tile.COL_BLANK_PERSON_WINDOW)
				tiles[i] = new Blank(Sprite.blank_tile_personWindow);
			else if(col == Tile.COL_BLANK_PERSON_EMPTY_WINDOW)
				tiles[i] = new Blank(Sprite.blank_tile_personEmptyWindow);
			else if(col == Tile.COL_PLANT)
				tiles[i] = new Blank(Sprite.plant);
			else if(col == Tile.COL_PLANT_WINDOW)
				tiles[i] = new Blank(Sprite.blank_tile_plantWindow);
			else if(col == 0xFFFFFFFF)
				tiles[i] = new Blank(Sprite.blank_tile);
			else
				tiles[i] = new LocalTeleportTile(new AnimatedSprite(SpriteSheet.teleporter_colored_animated, 0, 0, 16, 3), col);
		}
	}
	
	public void handleColorChange()
	{
		// Handles mouse-clicks for color changing
		
		if(Mouse.getButton() == 1 && player != null)
		{
			int newCol = Player.selectedColor;
			
			int mx = (Mouse.getScaledX() + Game.screen.getXOffset()) / 16;
			int my = (Mouse.getScaledY() + Game.screen.getYOffset()) / 16;
			
			try
			{
				if(totalColored <= MAX_COLORED)
				{
					if(tiles[mx+my*width_tiles].getTileData().COLOR != 0xFFFFFFFF && (totalColored == MAX_COLORED)) // If the tile is already colored, just allow the color change without adding to the totalCored. This prevents a glitch that stops you from changing the color of tiles after you reach the max 
					{
						tiles[mx+my*width_tiles].setColor(newCol);
					}
					else if(totalColored < MAX_COLORED)
					{
						totalColored += tiles[mx+my*width_tiles].setColor(newCol);
					}
				}
					
			}
			catch(Exception e)
			{
				
			}
		}
		else if(Mouse.getButton() == 3 && player != null)
		{
			int newCol = 0xFFFFFFFF;
			
			int mx = (Mouse.getScaledX() + Game.screen.getXOffset()) / 16;
			int my = (Mouse.getScaledY() + Game.screen.getYOffset()) / 16;
			
			try
			{
				totalColored += tiles[mx+my*width_tiles].setColor(newCol);
			}
			catch(Exception e)
			{
				
			}
		}
	}
	
	public void removeTeleporters()
	{
		for(int i = 0; i < tiles.length; i++)
		{
			if(tiles[i].getTileData().TELEPORTER && tiles[i].getTileData().COLOR != 0xFF00AD11)
			{
				tiles[i] = new Blank(Sprite.blank_tile);
			}
		}
	}
	
	public void addPlayer(Player player)
	{
		if(this.player == null)
		{
			this.player = player;
		}
		else
		{
			System.out.println("Player already added!");
		}
	}
	
	public Player getPlayer()
	{
		return player;
	}
	
	public void addEntity(Entity e)
	{
		entities.add(e);
	}
	
	public void removeEntity(int index)
	{
		entities.remove(index);
	}
	
	public void addGameFont(GameFont gf)
	{
		gameFont.add(gf);
	}
	
	public void removeGameFont(int index)
	{
		gameFont.remove(index);
	}
	
	public Tile getTile(int i)
	{
		return tiles[i];
	}
	
	public Tile getTile(int x, int y)
	{
		return tiles[x+y*width_tiles];
	}
	
	public TileData getTileData(int i)
	{
		TileData td = null;
		
		try
		{
			td = tiles[i].getTileData();
		}
		catch(Exception e)
		{
			td = new TileData(true, 0xFFFFFFFF, false);
		}
		
		return td;
	}
	
	public TileData getTileData(int x, int y)
	{
		TileData td = null;
		
		try
		{
			td = tiles[x+y*width_tiles].getTileData();
		}
		catch(Exception e)
		{
			td = new TileData(true, 0xFFFFFFFF, false);
		}
		
		return td;
	}
	
	public int getTileNumber(int x, int y)
	{
		 return (x+y*width_tiles);
	}
	
	public void setTileColor(int x, int y, int col)
	{
		tiles[x+y*width_tiles].setColor(col);
	}
	
	public int getTotalColored()
	{
		return totalColored;
	}
}
