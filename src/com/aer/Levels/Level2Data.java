package com.aer.Levels;

import java.util.ArrayList;

import com.aer.Entity.Player;
import com.aer.Main.Game;
import com.aer.Util.GameFont;

public class Level2Data extends LevelData
{
	private ArrayList<GameFont> gameFont = new ArrayList<GameFont>();
	
	private String[] text = { "Subject 147 seems to display basic", "knowledge of how to move.", "Impressive.", "Press 'r' to restart", "a level if you become trapped.", "You can press the escape key to go back", "to the main menu at any time. There you can load", "any level that you have reached thus far."};
	
	private int timer_text1 = 320;
	private boolean text1_activated = true;

	
	public Level getLevel()
	{
		return new Level("/res/textures/Levels/Level2.png", 0);
	}

	public int[] getSpawnCoords() 
	{
		int[] coords = { 150, 420 };
		return coords;
	}
	
	public Level2Data()
	{
		if(!Player.hasCompletedExtraLevels)
		{
			Player.unlockedBlue = false;
			Player.unlockedOrange = false;
			Player.unlockedPurple = false;
			Player.unlockedLBlue = false;
			Player.unlockedGreen = false;
			
			Player.totalUnlockedColors = 0;
		}
		else
		{
			Player.unlockedBlue = true;
			Player.unlockedOrange = true;
			Player.unlockedPurple = true;
			Player.unlockedLBlue = true;
			Player.unlockedGreen = true;
			
			Player.totalUnlockedColors = 5;
		}
		
		determineColors();
	}
	
	public void tick(Player player)
	{
		gameFont.clear();
		tickTimers();
		
		if(text1_activated)
		{
			gameFont.add(new GameFont(text[0], Game.width/3, Game.height/8, 0xFF0026FF, 0, 0, -1, true));
			gameFont.add(new GameFont(text[1], (int)(Game.width/2.7), Game.height/6, 0xFF0026FF, 0, 0, -1, true));
			gameFont.add(new GameFont(text[2], (int)(Game.width/2.2), (int)(Game.height/4.7), 0xFF0026FF, 0, 0, -1, true));
		}
		
		if(player.x > 120 && player.x < 200)
		{
			gameFont.add(new GameFont(text[3], Game.width/2, (int)(Game.height/2.5), 0xFFFF0000, 0, 0, -1, true));
			gameFont.add(new GameFont(text[4], Game.width/2, (int)(Game.height/2.3), 0xFFFF0000, 0, 0, -1, true));
		}
		
		if(player.x > 606 && player.x < 780)
		{
			gameFont.add(new GameFont(text[5], Game.width/5, Game.height/8, 0xFFFF0000, 0, 0, -1, true));
			gameFont.add(new GameFont(text[6], Game.width/5, (int)(Game.height/6.5), 0xFFFF0000, 0, 0, -1, true));
			gameFont.add(new GameFont(text[7], Game.width/5, (int)(Game.height/5.55), 0xFFFF0000, 0, 0, -1, true));
		}
	}
	
	private void tickTimers()
	{
		if(text1_activated)
		{
			if(timer_text1 > 0)
				timer_text1--;
			else
			{
				text1_activated = false;
			}
		}
	}
	
	public void render()
	{
		for(int i = 0; i < gameFont.size(); i++)
		{
			gameFont.get(i).render();
		}
	}
}
