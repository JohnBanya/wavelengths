package com.aer.Levels;

import java.util.ArrayList;

import com.aer.Entity.Player;
import com.aer.Util.GameFont;

public class Level17Data extends LevelData 
{
	private ArrayList<GameFont> gameFont = new ArrayList<GameFont>();
	
	public Level getLevel() 
	{
		return new Level("/res/textures/Levels/Level17.png", 12);
	}

	public int[] getSpawnCoords() 
	{
		int[] coords = { 76, 455 };
		return coords;
	}
	
	public Level17Data()
	{
		if(!Player.hasCompletedExtraLevels)
		{
			Player.unlockedBlue = true;
			Player.unlockedOrange = true;
			Player.unlockedPurple = true;
			Player.unlockedLBlue = true;
			Player.unlockedGreen = false;
			
			Player.totalUnlockedColors = 4;
		}
		else
		{
			Player.unlockedBlue = true;
			Player.unlockedOrange = true;
			Player.unlockedPurple = true;
			Player.unlockedLBlue = true;
			Player.unlockedGreen = true;
			
			Player.totalUnlockedColors = 5;
		}
		
		determineColors();
	}
	
	public void tick(Player player)
	{
		gameFont.clear();
	}
	
	public void render()
	{
		for(int i = 0; i < gameFont.size(); i++)
		{
			gameFont.get(i).render();
		}
	}

}
