package com.aer.Levels;

import java.util.ArrayList;

import com.aer.Entity.Player;
import com.aer.Util.GameFont;

public class Level11Data extends LevelData 
{
	private ArrayList<GameFont> gameFont = new ArrayList<GameFont>();
	
	public Level getLevel() 
	{
		return new Level("/res/textures/Levels/Level11.png", 12);
	}

	public int[] getSpawnCoords() 
	{
		int[] coords = { 76, 235 };
		return coords;
	}
	
	public Level11Data()
	{
		if(!Player.hasCompletedExtraLevels)
		{
			Player.unlockedBlue = true;
			Player.unlockedOrange = true;
			Player.unlockedPurple = false;
			Player.unlockedLBlue = false;
			Player.unlockedGreen = false;
			
			Player.totalUnlockedColors = 2;
		}
		else
		{
			Player.unlockedBlue = true;
			Player.unlockedOrange = true;
			Player.unlockedPurple = true;
			Player.unlockedLBlue = true;
			Player.unlockedGreen = true;
			
			Player.totalUnlockedColors = 5;
		}
		
		determineColors();
	}
	
	public void tick(Player player)
	{
		gameFont.clear();
	}
	
	public void render()
	{
		for(int i = 0; i < gameFont.size(); i++)
		{
			gameFont.get(i).render();
		}
	}
}
