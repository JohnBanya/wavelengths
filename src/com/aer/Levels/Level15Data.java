package com.aer.Levels;

import java.util.ArrayList;

import com.aer.Entity.Player;
import com.aer.Main.Game;
import com.aer.Util.GameFont;

public class Level15Data extends LevelData 
{
	private ArrayList<GameFont> gameFont = new ArrayList<GameFont>();

	private String[] text = { "Congratulations on surviving thus far.", "You have exceeded all of our expectations!", "If you survive this next test, I'll introduce.", "you to our highly classified new discovery!"};
	private int[] teleColors = { 0xFF7F92FF, 236, 6120, 0xFF7B8EF7, 62, 319 };
	
	private int timer_text1 = 320;
	private boolean text1_activated = true;
	
	private int timer_text2 = 320;
	private boolean text2_activated = false;
	
	public Level getLevel() 
	{
		return new Level("/res/textures/Levels/Level15.png", 3);
	}

	public int[] getSpawnCoords() 
	{
		int[] coords = { 178, 280 };
		return coords;
	}
	
	public Level15Data()
	{
		if(!Player.hasCompletedExtraLevels)
		{
			Player.unlockedBlue = true;
			Player.unlockedOrange = true;
			Player.unlockedPurple = true;
			Player.unlockedLBlue = false;
			Player.unlockedGreen = false;
			
			Player.totalUnlockedColors = 3;
		}
		else
		{
			Player.unlockedBlue = true;
			Player.unlockedOrange = true;
			Player.unlockedPurple = true;
			Player.unlockedLBlue = true;
			Player.unlockedGreen = true;
			
			Player.totalUnlockedColors = 5;
		}
	
		determineColors();
	}
	
	public void tick(Player player)
	{
		gameFont.clear();
		tickTimers();
		
		if(text1_activated)
		{
			gameFont.add(new GameFont(text[0], Game.width/3, Game.height/8, 0xFF0026FF, 0, 0, -1, true));
			gameFont.add(new GameFont(text[1], (int)(Game.width/2.9), Game.height/6, 0xFF0026FF, 0, 0, -1, true));
		}
	
		if(text2_activated)
		{
			gameFont.add(new GameFont(text[2], Game.width/3, Game.height/8, 0xFF0026FF, 0, 0, -1, true));
			gameFont.add(new GameFont(text[3], (int)(Game.width/2.9), Game.height/6, 0xFF0026FF, 0, 0, -1, true));
		}
		
		if(player.isReadyToTeleport())
		{
			int teleColor = player.getTeleportColor();
			for(int i = 0; i < teleColors.length; i+=3)
			{
				if(teleColors[i] == teleColor)
				{
					player.x = teleColors[i+1];
					player.y = teleColors[i+2];
					player.resetTeleport();
					i = teleColors.length; // end loop;
				}
			}
		}
		
	}
	
	private void tickTimers()
	{
		if(text1_activated)
		{
			if(timer_text1 > 0)
				timer_text1--;
			else
			{
				text1_activated = false;
				text2_activated = true;
			}
		}
		
		if(text2_activated)
		{
			if(timer_text2 > 0)
				timer_text2--;
			else
			{
				text2_activated = false;
			}
		}
	}
	
	public void render()
	{
		for(int i = 0; i < gameFont.size(); i++)
		{
			gameFont.get(i).render();
		}
	}

}
