package com.aer.Levels;

import com.aer.Entity.Player;
import com.aer.Main.Game;
import com.aer.Main.Keyboard;

public class LevelLoader 
{
	private Keyboard key;
	private Player player;
	
	private final int MAX_LEVELS = 30;
	private int currentLevelNumber = 1;
	
	private Level currentLevel;
	private LevelData currentLevelData;
	
	private boolean teleportersRemoved = false; // used to increase fps after teleporters are removed (stops scanning of teleporters present in level multiple times)
	
	/*
	 Levels 1-30 = Normal Levels
	 Level -100 = Plant Level
	 */
	
	
	public LevelLoader(Keyboard key)		// Constructor that accepts a Keyboard object 
	{
		this.key = key;
		loadCurrentLevel();
	}
	
	public LevelLoader(Keyboard key, int level)			// Constructor that accepts a Keyboard object and a level number (to load a specific level)
	{	
		this.key = key;
		currentLevelNumber = level;
		loadCurrentLevel();
	}
	
	public void tick()
	{
		// Run tick methods (60 times per second)
		currentLevel.tick();
		currentLevelData.tick(player);
		
		// Used in timed levels (when countdown timer reaches zero, currentLevelData.getTeleporterShuntdown() is 'true')
		if(currentLevelData.getTeleporterShutdown() && !teleportersRemoved)
		{
			currentLevel.removeTeleporters();
			teleportersRemoved = true;
		}
		
		// If the player presses the level reset button 'r', reload the level
		if(player.hasRequestReset())
			loadCurrentLevel();
		
		// When the player has requested the next level ('true' when player reaches next level teleporter)
		if(player.hasRequestedNextLevel())
		{
			if(currentLevel.MAX_COLORED > 0) // Calculate % complete based on the amount of tiles used in the completed level
			{
				int percentComplete = (int) ((1 - ( (currentLevel.totalColored * 1.0) / (currentLevel.MAX_COLORED * 1.0)) ) * 100);
				
				if(percentComplete == 100 && currentLevelNumber > 0 && currentLevelNumber <= Player.levelRating.length)
				{
					Player.levelRating[currentLevelNumber-1] = 5;
				}
				else if(percentComplete >= 80 && percentComplete < 100 && currentLevelNumber > 0 && currentLevelNumber <= Player.levelRating.length)
				{
					Player.levelRating[currentLevelNumber-1] = 4;
				}
				else if(percentComplete >= 50 && percentComplete < 80 && currentLevelNumber > 0 && currentLevelNumber <= Player.levelRating.length)
				{
					Player.levelRating[currentLevelNumber-1] = 3;
				}
				else if(percentComplete >= 20 && percentComplete < 50 && currentLevelNumber > 0 && currentLevelNumber <= Player.levelRating.length)
				{
					Player.levelRating[currentLevelNumber-1] = 2;
				}
				else if(currentLevelNumber > 0 && currentLevelNumber <= Player.levelRating.length)
				{
					Player.levelRating[currentLevelNumber-1] = 1;
				}
			}
			else	// When the maximum colored tiles is equal to 0, always give 5 stars
			{
				if(currentLevelNumber > 0 && currentLevelNumber <= Player.levelRating.length) // When the player is between level 1 and the maximum level
				{
					Player.levelRating[currentLevelNumber-1] = 5; // Set value of the array to 5 (5 star rating)
				}
			}
			
			nextLevel();
		}
			
		if(key.key_esc)
		{
			Game.credits.setActivated(false);
			Game.menu.setActivated(true);
		}
		
		// Special Teleport cases
		if(player.getTeleportColor() == 0xFFA000EA && player.isReadyToTeleport()) // End Game
		{
			Game.credits.setEscaped(false);
			Game.credits.setActivated(true);
		}
		
		if(player.getTeleportColor() == 0xFF00AD11 && player.isReadyToTeleport()) // Start Plant Level
		{
			currentLevelNumber = -100; // Hidden levels are measured in negative to prevent interference with regular level progression.
			loadCurrentLevel();
		}
		
		if(player.getTeleportColor() == 0xFF00A010 && player.isReadyToTeleport()) // End Plant levels
		{
			Player.hasCompletedExtraLevels = true;
			currentLevelNumber = 29;
			loadCurrentLevel();
		}
		
		if(player.getTeleportColor() == 0xFFBC9D00 && player.isReadyToTeleport()) // Start Pyramid Level
		{
			currentLevelNumber = -99; // Hidden levels are measured in negative to prevent interference with regular level progression.
			loadCurrentLevel();
		}
		
	}
	
	public void render()
	{
		// Run render methods
		currentLevel.render();
		currentLevelData.render();
	}
	
	public void loadCurrentLevel()
	{
		// Reset the current level and the player
		currentLevel = null; // Level object
		player = null;	// Player object
		
		// Get the currentLevelData (contains level information) by calling the getCurrentLevelData() method
		currentLevelData = getCurrentLevelData();
		
		// Get the level object from the Level Data
		currentLevel = currentLevelData.getLevel();
		
		// Create a new player based on the spawn coordinates from level data (from a returned array, with value 0 being the x coordinate and value 1 being the y)
		player = new Player(currentLevelData.getSpawnCoords()[0], currentLevelData.getSpawnCoords()[1], currentLevel, key);
		
		// Add player to the level
		currentLevel.addPlayer(player);
		
		// Create teleporters
		teleportersRemoved = false;
	}
	
	public LevelData getCurrentLevelData()
	{
		// Returns Level Data based on the current level number integer(default: 1)
		
		if(currentLevelNumber == 1)
			return new Level1Data();
		else if(currentLevelNumber == 2)
			return new Level2Data();
		else if(currentLevelNumber == 3)
			return new Level3Data();
		else if(currentLevelNumber == 4)
			return new Level4Data();
		else if(currentLevelNumber == 5)
			return new Level5Data();
		else if(currentLevelNumber == 6)
			return new Level6Data();
		else if(currentLevelNumber == 7)
			return new Level7Data();
		else if(currentLevelNumber == 8)
			return new Level8Data();
		else if(currentLevelNumber == 9)
			return new Level9Data();
		else if(currentLevelNumber == 10)
			return new Level10Data();
		else if(currentLevelNumber == 11)
			return new Level11Data();
		else if(currentLevelNumber == 12)
			return new Level12Data();
		else if(currentLevelNumber == 13)
			return new Level13Data();
		else if(currentLevelNumber == 14)
			return new Level14Data();
		else if(currentLevelNumber == 15)
			return new Level15Data();
		else if(currentLevelNumber == 16)
			return new Level16Data();
		else if(currentLevelNumber == 17)
			return new Level17Data();
		else if(currentLevelNumber == 18)
			return new Level18Data();
		else if(currentLevelNumber == 19)
			return new Level19Data();
		else if(currentLevelNumber == 20)
			return new Level20Data();
		else if(currentLevelNumber == 21)
			return new Level21Data();
		else if(currentLevelNumber == 22)
			return new Level22Data();
		else if(currentLevelNumber == 23)
			return new Level23Data();
		else if(currentLevelNumber == 24)
			return new Level24Data();
		else if(currentLevelNumber == 25)
			return new Level25Data();
		else if(currentLevelNumber == 26)
			return new Level26Data();
		else if(currentLevelNumber == 27)
			return new Level27Data();
		else if(currentLevelNumber == 28)
			return new Level28Data();
		else if(currentLevelNumber == 29)
			return new Level29Data();
		else if(currentLevelNumber == 30)
			return new Level30Data();
		else if(currentLevelNumber == -100)
			return new LevelPlantData();
		else // Default: return level 1 data
			return new Level1Data();
	}
	
	public int getCurrentLevelNumber()
	{
		// Returns the integer value of the currentLevelNumber (gives level number)
		return currentLevelNumber;
	}
	
	public void setCurrentLevelNumber(int number)
	{
		// Set the current level number
		currentLevelNumber = number;
	}
	
	public void nextLevel()
	{
		if(currentLevelNumber < MAX_LEVELS)
		{
			if(currentLevelNumber >= 0 && currentLevelNumber < Player.unlockedLevel.length)
				Player.unlockedLevel[currentLevelNumber] = true;
				
			currentLevelNumber++;
			loadCurrentLevel();
		}
		else
		{
			Game.credits.setEscaped(true);
			Game.credits.setActivated(true);
		}
	}
	
	public Player getPlayer()
	{
		return player;
	}
	
	public double getPlayerX()
	{
		return player.x;
	}
	
	public double getPlayerY()
	{
		return player.y;
	}
}
