package com.aer.Levels;

import java.util.ArrayList;

import com.aer.Entity.Player;
import com.aer.Main.Game;
import com.aer.Util.GameFont;

public class Level16Data extends LevelData
{
	private ArrayList<GameFont> gameFont = new ArrayList<GameFont>();
	
	private String[] text = { "You have unlocked the color light-blue!", "Our data predicts that this new wavelength", "should distort the molecules of a solid to the point", "where it acts as a liquid under high enough pressures.", "We have never had a test subject make it this", "far into the tests. You, Subject 147, will go down", "in history as the first person to test wavelength 494!"};
	
	private int timer_text1 = 480;
	private boolean text1_activated = true;
	
	private int timer_text2 = 480;
	private boolean text2_activated = false;
	
	public Level getLevel() 
	{
		return new Level("/res/textures/Levels/Level16.png", 3);
	}

	public int[] getSpawnCoords() 
	{
		int[] coords = { 166, 455 };
		return coords;
	}
	
	public Level16Data()
	{
		if(!Player.hasCompletedExtraLevels)
		{
			Player.unlockedBlue = true;
			Player.unlockedOrange = true;
			Player.unlockedPurple = true;
			Player.unlockedLBlue = true;
			Player.unlockedGreen = false;
			
			Player.totalUnlockedColors = 4;
		}
		else
		{
			Player.unlockedBlue = true;
			Player.unlockedOrange = true;
			Player.unlockedPurple = true;
			Player.unlockedLBlue = true;
			Player.unlockedGreen = true;
			
			Player.totalUnlockedColors = 5;
		}
		
		determineColors();
	}
	
	public void tick(Player player)
	{
		gameFont.clear();
		tickTimers();
		
		
		if(player.x > 27 && player.x < 320)
		{
			gameFont.add(new GameFont(text[0], (int)(Game.width/2.3), (int)(Game.height/3), 0xFF7CCAFF, 0, 0, -1, true));
		}
		
		if(text1_activated)
		{
			
			gameFont.add(new GameFont(text[1], Game.width/4, Game.height/8, 0xFF0026FF, 0, 0, -1, true));
			gameFont.add(new GameFont(text[2], (int)(Game.width/4), Game.height/6, 0xFF0026FF, 0, 0, -1, true));
			gameFont.add(new GameFont(text[3], (int)(Game.width/4), (int)(Game.height/4.8), 0xFF0026FF, 0, 0, -1, true));
		}
		
		if(text2_activated)
		{
			gameFont.add(new GameFont(text[4], Game.width/4, Game.height/8, 0xFF0026FF, 0, 0, -1, true));
			gameFont.add(new GameFont(text[5], (int)(Game.width/4), Game.height/6, 0xFF0026FF, 0, 0, -1, true));
			gameFont.add(new GameFont(text[6], (int)(Game.width/4), (int)(Game.height/4.8), 0xFF0026FF, 0, 0, -1, true));
		}
	}
	
	private void tickTimers()
	{
		if(text1_activated)
		{
			if(timer_text1 > 0)
				timer_text1--;
			else
			{
				text1_activated = false;
				text2_activated = true;
			}
		}
		
		if(text2_activated)
		{
			if(timer_text2 > 0)
				timer_text2--;
			else
			{
				text2_activated = false;
			}
		}
	}
	
	public void render()
	{
		for(int i = 0; i < gameFont.size(); i++)
		{
			gameFont.get(i).render();
		}
	}

}
