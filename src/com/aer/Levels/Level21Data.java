package com.aer.Levels;

import java.util.ArrayList;

import com.aer.Entity.Player;
import com.aer.Main.Game;
import com.aer.Util.GameFont;

public class Level21Data extends LevelData 
{
	private ArrayList<GameFont> gameFont = new ArrayList<GameFont>();
	
	private String[] text = { "You made it! You've completed the tests!", "With the data we've collected, I think we will be able to", "  completely change life as we know it. Your efforts have", "    revolutionized modern science! We can change the", "world, all thanks to you.",
			"I really cannot thank you enough.", "There is just one problem though... ", "I can't let you leave. You're too valuable.", "All of our previous test subjects have been killed", "by prolonged exposure to wavelength 494. You survived.", "In some way, you are different than everybody else.", "If you leave, we may never find out what makes you special.", "That's just too big of a risk to take.", "Just enter through that purple teleporter ahead of you.", "Your family will be told you were killed in a car accident.", "Go on, go right on through."};
	
	private int timer_text1 = 280;
	private boolean text1_activated = true;
	
	private int timer_text2 = 560;
	private boolean text2_activated = false;
	
	private int timer_text3 = 280;
	private boolean text3_activated = false;
	
	private int timer_text4 = 280;
	private boolean text4_activated = false;
	
	private int timer_text5 = 520;
	private boolean text5_activated = false;
	
	private int timer_text6 = 560;
	private boolean text6_activated = false;
	
	private int timer_text7 = 560;
	private boolean text7_activated = false;
	
	
	public Level getLevel() 
	{
		return new Level("/res/textures/Levels/Level21.png", 16);
	}

	public int[] getSpawnCoords() 
	{
		int[] coords = { 200, 300 };
		return coords;
	}
	
	public Level21Data()
	{
		if(!Player.hasCompletedExtraLevels)
		{
			Player.unlockedBlue = true;
			Player.unlockedOrange = true;
			Player.unlockedPurple = true;
			Player.unlockedLBlue = true;
			Player.unlockedGreen = false;
			
			Player.totalUnlockedColors = 4;
		}
		else
		{
			Player.unlockedBlue = true;
			Player.unlockedOrange = true;
			Player.unlockedPurple = true;
			Player.unlockedLBlue = true;
			Player.unlockedGreen = true;
			
			Player.totalUnlockedColors = 5;
		}
		
		determineColors();
	}
	
	public void tick(Player player)
	{
		gameFont.clear();
		tickTimers();
		
		if(text1_activated)
		{
			gameFont.add(new GameFont(text[0], Game.width/3, Game.height/8, 0xFF0026FF, 0, 0, -1, true));
		}
	
		if(text2_activated)
		{
			gameFont.add(new GameFont(text[1], Game.width/5, Game.height/8, 0xFF0026FF, 0, 0, -1, true));
			gameFont.add(new GameFont(text[2], (int)(Game.width/5), (int)(Game.height/6.5), 0xFF0026FF, 0, 0, -1, true));
			gameFont.add(new GameFont(text[3], (int)(Game.width/5), (int)(Game.height/5.55), 0xFF0026FF, 0, 0, -1, true));
			gameFont.add(new GameFont(text[4], (int)(Game.width/3), (int)(Game.height/4.8), 0xFF0026FF, 0, 0, -1, true));
		}
		
		if(text3_activated)
		{
			gameFont.add(new GameFont(text[5], Game.width/3, Game.height/8, 0xFF0026FF, 0, 0, -1, true));
		}
		
		if(text4_activated)
		{
			gameFont.add(new GameFont(text[6], Game.width/3, Game.height/8, 0xFF0026FF, 0, 0, -1, true));
		}
		
		if(text5_activated)
		{
			gameFont.add(new GameFont(text[7], (int)(Game.width/5), Game.height/8, 0xFF0026FF, 0, 0, -1, true));
			gameFont.add(new GameFont(text[8], Game.width/5, (int)(Game.height/6.5), 0xFF0026FF, 0, 0, -1, true));
			gameFont.add(new GameFont(text[9], (int)(Game.width/5), (int)(Game.height/5.55), 0xFF0026FF, 0, 0, -1, true));
		}
		
		if(text6_activated)
		{
			gameFont.add(new GameFont(text[10], (int)(Game.width/5), Game.height/8, 0xFF0026FF, 0, 0, -1, true));
			gameFont.add(new GameFont(text[11], Game.width/5, (int)(Game.height/6.5), 0xFF0026FF, 0, 0, -1, true));
			gameFont.add(new GameFont(text[12], (int)(Game.width/5), (int)(Game.height/5.55), 0xFF0026FF, 0, 0, -1, true));
		}
		
		if(text7_activated)
		{
			gameFont.add(new GameFont(text[13], (int)(Game.width/5), Game.height/8, 0xFF0026FF, 0, 0, -1, true));
			gameFont.add(new GameFont(text[14], (int)(Game.width/5), (int)(Game.height/6.5), 0xFF0026FF, 0, 0, -1, true));
			gameFont.add(new GameFont(text[15], (int)(Game.width/5), (int)(Game.height/5.55), 0xFF0026FF, 0, 0, -1, true));
		}
		
	}
	
	private void tickTimers()
	{
		if(text1_activated)
		{
			if(timer_text1 > 0)
				timer_text1--;
			else
			{
				text1_activated = false;
				text2_activated = true;
			}
		}
		
		if(text2_activated)
		{
			if(timer_text2 > 0)
				timer_text2--;
			else
			{
				text2_activated = false;
				text3_activated = true;
			}
		}
		
		if(text3_activated)
		{
			if(timer_text3 > 0)
				timer_text3--;
			else
			{
				text3_activated = false;
				text4_activated = true;
			}
		}
		
		if(text4_activated)
		{
			if(timer_text4 > 0)
				timer_text4--;
			else
			{
				text4_activated = false;
				text5_activated = true;
			}
		}
		
		if(text5_activated)
		{
			if(timer_text5 > 0)
				timer_text5--;
			else
			{
				text5_activated = false;
				text6_activated = true;
			}
		}
		
		if(text6_activated)
		{
			if(timer_text6 > 0)
				timer_text6--;
			else
			{
				text6_activated = false;
				text7_activated = true;
			}
		}
		
		if(text7_activated)
		{
			if(timer_text7 > 0)
				timer_text7--;
			else
			{
				text7_activated = false;
			}
		}
	}
	
	public void render()
	{
		for(int i = 0; i < gameFont.size(); i++)
		{
			gameFont.get(i).render();
		}
	}
}
