package com.aer.Levels;

import java.util.ArrayList;

import com.aer.Entity.Player;
import com.aer.Util.GameFont;

public class Level20Data extends LevelData 
{
	private ArrayList<GameFont> gameFont = new ArrayList<GameFont>();
	private int[] teleColors = { 0xFFB200FF, 142, 2655, 0xFFA900F7, 470, 3135 };
	
	public Level getLevel() 
	{
		return new Level("/res/textures/Levels/Level20.png", 1);
	}

	public int[] getSpawnCoords() 
	{
		int[] coords = { 76, 1640 };
		return coords;
	}
	
	public Level20Data()
	{
		if(!Player.hasCompletedExtraLevels)
		{
			Player.unlockedBlue = true;
			Player.unlockedOrange = true;
			Player.unlockedPurple = true;
			Player.unlockedLBlue = true;
			Player.unlockedGreen = false;
			
			Player.totalUnlockedColors = 4;
		}
		else
		{
			Player.unlockedBlue = true;
			Player.unlockedOrange = true;
			Player.unlockedPurple = true;
			Player.unlockedLBlue = true;
			Player.unlockedGreen = true;
			
			Player.totalUnlockedColors = 5;
		}
		
		determineColors();
	}
	
	public void tick(Player player)
	{
		gameFont.clear();
		
		if(player.isReadyToTeleport())
		{
			int teleColor = player.getTeleportColor();
			for(int i = 0; i < teleColors.length; i+=3)
			{
				if(teleColors[i] == teleColor)
				{
					player.x = teleColors[i+1];
					player.y = teleColors[i+2];
					player.resetTeleport();
					i = teleColors.length; // end loop;
				}
			}
		}
	}
	
	public void render()
	{
		for(int i = 0; i < gameFont.size(); i++)
		{
			gameFont.get(i).render();
		}
	}
}
