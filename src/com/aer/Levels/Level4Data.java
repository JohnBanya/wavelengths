package com.aer.Levels;

import java.util.ArrayList;

import com.aer.Entity.Player;
import com.aer.Main.Game;
import com.aer.Util.GameFont;

public class Level4Data extends LevelData
{
	private ArrayList<GameFont> gameFont = new ArrayList<GameFont>();
	
	private String[] text = { "Different colored tiles", "have different properties."};
	
	
	public Level getLevel()
	{
		return new Level("/res/textures/Levels/Level4.png", 0);
	}

	public int[] getSpawnCoords() 
	{
		int[] coords = { 54, 560 };
		return coords;
	}
	
	public Level4Data()
	{
		if(!Player.hasCompletedExtraLevels)
		{
			Player.unlockedBlue = false;
			Player.unlockedOrange = false;
			Player.unlockedPurple = false;
			Player.unlockedLBlue = false;
			Player.unlockedGreen = false;
			
			Player.totalUnlockedColors = 0;
		}
		else
		{
			Player.unlockedBlue = true;
			Player.unlockedOrange = true;
			Player.unlockedPurple = true;
			Player.unlockedLBlue = true;
			Player.unlockedGreen = true;
			
			Player.totalUnlockedColors = 5;
		}
		
		determineColors();
	}
	
	public void tick(Player player)
	{
		gameFont.clear();
		
		if(player.x > 27 && player.x < 100 && player.y > 490 && player.y < 580)
		{
			gameFont.add(new GameFont(text[0], Game.width/2, (int)(Game.height/2.5), 0xFFFF0000, 0, 0, -1, true));
			gameFont.add(new GameFont(text[1], Game.width/2, (int)(Game.height/2.3), 0xFFFF0000, 0, 0, -1, true));
		}
	}
	
	public void render()
	{
		for(int i = 0; i < gameFont.size(); i++)
		{
			gameFont.get(i).render();
		}
	}
}
