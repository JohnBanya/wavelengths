package com.aer.Levels;

import java.util.ArrayList;

import com.aer.Entity.Player;
import com.aer.Main.Game;
import com.aer.Util.GameFont;

public class Level22Data extends LevelData 
{
	private ArrayList<GameFont> gameFont = new ArrayList<GameFont>();
	
	private String[] text = { "Wait! What are you doing?...! You can't go that way!", "If you leave, everything you've done for us", "will have been pointless!", "Quick! Shut down all teleporters!"};
	
	private int timer_text1 = 320;
	private boolean text1_activated = true;
	
	private int timer_text2 = 360;
	private boolean text2_activated = false;
	
	private int timer_text3 = 380;
	private boolean text3_activated = false;
	
	private boolean timerActivated = false;
	private int timer = 660;
	
	private boolean teleporterShutdown = false;
	
	
	public Level getLevel() 
	{
		return new Level("/res/textures/Levels/Level22.png", 16);
	}

	public int[] getSpawnCoords() 
	{
		int[] coords = { 110, 450 };
		return coords;
	}
	
	public Level22Data()
	{
		if(!Player.hasCompletedExtraLevels)
		{
			Player.unlockedBlue = true;
			Player.unlockedOrange = true;
			Player.unlockedPurple = true;
			Player.unlockedLBlue = true;
			Player.unlockedGreen = false;
			
			Player.totalUnlockedColors = 4;
		}
		else
		{
			Player.unlockedBlue = true;
			Player.unlockedOrange = true;
			Player.unlockedPurple = true;
			Player.unlockedLBlue = true;
			Player.unlockedGreen = true;
			
			Player.totalUnlockedColors = 5;
		}
		
		Player.selectedColor = Player.PLAYER_COL_BLUE;
	}
	
	public void tick(Player player)
	{
		gameFont.clear();
		tickTimers();
		
		if(text1_activated)
		{
			gameFont.add(new GameFont(text[0], Game.width/3, Game.height/8, 0xFF0026FF, 0, 0, -1, true));
		}
	
		if(text2_activated)
		{
			gameFont.add(new GameFont(text[1], Game.width/3, Game.height/8, 0xFF0026FF, 0, 0, -1, true));
			gameFont.add(new GameFont(text[2], (int)(Game.width/2.5), (int)(Game.height/6.5), 0xFF0026FF, 0, 0, -1, true));
		}
		
		if(text3_activated)
		{
			gameFont.add(new GameFont(text[3], Game.width/3, Game.height/8, 0xFF0026FF, 0, 0, -1, true));
		}
		
		if(timerActivated)
		{
			if(((int)timer/60) > 0)
				gameFont.add(new GameFont("Teleporter shutdown in " + ((int)timer/60), 150, 13, 0xFFFF0000, 0, 0, -1, true));
			else
			{
				gameFont.add(new GameFont("Teleporter shutdown." , 150, 13, 0xFFFF0000, 0, 0, -1, true));
				teleporterShutdown = true;
			}
		}
	}
	
	private void tickTimers()
	{
		if(timer > 0 && timerActivated)
		{
			timer--;
		}
		
		if(text1_activated)
		{
			if(timer_text1 > 0)
				timer_text1--;
			else
			{
				text1_activated = false;
				text2_activated = true;
			}
		}
		
		if(text2_activated)
		{
			if(timer_text2 > 0)
				timer_text2--;
			else
			{
				text2_activated = false;
				text3_activated = true;
			}
		}
		
		if(text3_activated)
		{
			if(timer_text3 > 0)
				timer_text3--;
			else
			{
				text3_activated = false;
				timerActivated = true;
			}
		}
	}
	
	public void render()
	{
		for(int i = 0; i < gameFont.size(); i++)
		{
			gameFont.get(i).render();
		}
	}
	
	public boolean getTeleporterShutdown()
	{
		return teleporterShutdown;
	}
}
