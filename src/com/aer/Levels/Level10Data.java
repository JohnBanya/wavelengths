package com.aer.Levels;

import java.util.ArrayList;

import com.aer.Entity.Player;
import com.aer.Main.Game;
import com.aer.Util.GameFont;

public class Level10Data extends LevelData
{
	private ArrayList<GameFont> gameFont = new ArrayList<GameFont>();
	
	private String[] text = { "You have unlocked the color Orange!", "You can change your selected", "color with the number keys"};
	
	public Level getLevel() 
	{
		return new Level("/res/textures/Levels/Level10.png", 15);
	}

	public int[] getSpawnCoords() 
	{
		int[] coords = { 76, 235 };
		return coords;
	}
	
	public Level10Data()
	{
		if(!Player.hasCompletedExtraLevels)
		{
			Player.unlockedBlue = true;
			Player.unlockedOrange = true;
			Player.unlockedPurple = false;
			Player.unlockedLBlue = false;
			Player.unlockedGreen = false;
			
			Player.totalUnlockedColors = 2;
		}
		else
		{
			Player.unlockedBlue = true;
			Player.unlockedOrange = true;
			Player.unlockedPurple = true;
			Player.unlockedLBlue = true;
			Player.unlockedGreen = true;
			
			Player.totalUnlockedColors = 5;
		}
		
		 determineColors();
	}
	
	public void tick(Player player)
	{
		gameFont.clear();
		
		if(player.x > 27 && player.x < 350)
		{
			gameFont.add(new GameFont(text[0], (int)(Game.width/2.5), (int)(Game.height/6), 0xFFFF6A00, 0, 0, -1, true));
			gameFont.add(new GameFont(text[1], (int)(Game.width/2.2), (int)(Game.height/5.0), 0xFFFF0000, 0, 0, -1, true));
			gameFont.add(new GameFont(text[2], (int)(Game.width/2.1), (int)(Game.height/4.5), 0xFFFF0000, 0, 0, -1, true));
		}
	}
	
	public void render()
	{
		for(int i = 0; i < gameFont.size(); i++)
		{
			gameFont.get(i).render();
		}
	}
}
