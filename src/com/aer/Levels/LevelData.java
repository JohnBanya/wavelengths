package com.aer.Levels;
import com.aer.Entity.Player;

public abstract class LevelData 
{	
	public abstract void tick(Player player);
	public abstract void render();
	
	public abstract Level getLevel();
	public abstract int[] getSpawnCoords();
	
	protected void determineColors()
	{		
		// Color index
		if(Player.colorIndex > Player.totalUnlockedColors)
		{
			Player.colorIndex = Player.totalUnlockedColors;
		}
		else if(Player.colorIndex < 1 && Player.totalUnlockedColors >= 1)
		{
			Player.colorIndex = 1;
		}
		else if(Player.totalUnlockedColors == 0)
		{
			Player.colorIndex = 0;
		}
		
		
		// Selected Colors
		if(Player.colorIndex == 1)
			Player.selectedColor = Player.PLAYER_COL_BLUE;
		else if(Player.colorIndex == 2)
			Player.selectedColor = Player.PLAYER_COL_ORANGE;
		else if(Player.colorIndex == 3)
			Player.selectedColor = Player.PLAYER_COL_PURPLE;
		else if(Player.colorIndex == 4)
			Player.selectedColor = Player.PLAYER_COL_LBLUE;
		else if(Player.colorIndex == 5)
			Player.selectedColor = Player.PLAYER_COL_GREEN;
		else
			Player.selectedColor = 0xFFFFFFFF;
	}
	
	public boolean getTeleporterShutdown()
	{
		return false;
	}
}
