package com.aer.Levels;

import java.util.ArrayList;

import com.aer.Entity.Player;
import com.aer.Main.Game;
import com.aer.Util.GameFont;

public class Level3Data extends LevelData
{
	private ArrayList<GameFont> gameFont = new ArrayList<GameFont>();
	
	private String[] text = { "Interesting. 95% of our subjects", "never make it past that last test.", "There still may be hope for you left.", "Initiating Operation Wavelengths..."};
	
	private int timer_text1 = 360;
	private boolean text1_activated = true;


	public Level getLevel()
	{
		return new Level("/res/textures/Levels/Level3.png", 0);
	}

	public int[] getSpawnCoords() 
	{
		int[] coords = { 120, 420 };
		return coords;
	}
	
	public Level3Data()
	{
		if(!Player.hasCompletedExtraLevels)
		{
			Player.unlockedBlue = false;
			Player.unlockedOrange = false;
			Player.unlockedPurple = false;
			Player.unlockedLBlue = false;
			Player.unlockedGreen = false;
			
			Player.totalUnlockedColors = 0;
		}
		else
		{
			Player.unlockedBlue = true;
			Player.unlockedOrange = true;
			Player.unlockedPurple = true;
			Player.unlockedLBlue = true;
			Player.unlockedGreen = true;
			
			Player.totalUnlockedColors = 5;
		}
		
		determineColors();
	}
	
	public void tick(Player player)
	{
		gameFont.clear();
		tickTimers();
	
		if(text1_activated)
		{
			gameFont.add(new GameFont(text[0], Game.width/3, Game.height/8, 0xFF0026FF, 0, 0, -1, true));
			gameFont.add(new GameFont(text[1], Game.width/3, Game.height/6, 0xFF0026FF, 0, 0, -1, true));
			gameFont.add(new GameFont(text[2], Game.width/3, (int)(Game.height/4.7), 0xFF0026FF, 0, 0, -1, true));
		}
		
		if(player.x > 88 && player.x < 152)
		{
			gameFont.add(new GameFont(text[3], Game.width/2, (int)(Game.height/2.5), 0xFFFF0000, 0, 0, -1, true));
		}
	}
	
	private void tickTimers()
	{
		if(text1_activated)
		{
			if(timer_text1 > 0)
				timer_text1--;
			else
			{
				text1_activated = false;
			}
		}
	}
	
	public void render()
	{
		for(int i = 0; i < gameFont.size(); i++)
		{
			gameFont.get(i).render();
		}
	}
}
