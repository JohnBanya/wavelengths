package com.aer.Levels;

import java.util.ArrayList;

import com.aer.Entity.Player;
import com.aer.Util.GameFont;

public class Level14Data extends LevelData 
{
	private ArrayList<GameFont> gameFont = new ArrayList<GameFont>();

	private int[] teleColors = { 0xFFFF7FED, 45, 2575, 0xFFFC7EEB, 45, 6280 };
	
	public Level getLevel() 
	{
		return new Level("/res/textures/Levels/Level14.png", 18);
	}

	public int[] getSpawnCoords() 
	{
		int[] coords = { 70, 2575 };
		return coords;
	}
	
	public Level14Data()
	{
		if(!Player.hasCompletedExtraLevels)
		{
			Player.unlockedBlue = true;
			Player.unlockedOrange = true;
			Player.unlockedPurple = true;
			Player.unlockedLBlue = false;
			Player.unlockedGreen = false;
			
			Player.totalUnlockedColors = 3;
		}
		else
		{
			Player.unlockedBlue = true;
			Player.unlockedOrange = true;
			Player.unlockedPurple = true;
			Player.unlockedLBlue = true;
			Player.unlockedGreen = true;
			
			Player.totalUnlockedColors = 5;
		}
		
		determineColors();
	}
	
	public void tick(Player player)
	{
		gameFont.clear();
		
		if(player.isReadyToTeleport())
		{
			int teleColor = player.getTeleportColor();
			for(int i = 0; i < teleColors.length; i+=3)
			{
				if(teleColors[i] == teleColor)
				{
					player.x = teleColors[i+1];
					player.y = teleColors[i+2];
					player.resetTeleport();
					i = teleColors.length; // end loop;
				}
			}
		}
		
	}
	
	public void render()
	{
		for(int i = 0; i < gameFont.size(); i++)
		{
			gameFont.get(i).render();
		}
	}
}
