package com.aer.Levels;

import java.util.ArrayList;

import com.aer.Entity.Player;
import com.aer.Main.Game;
import com.aer.Util.GameFont;

public class LevelPlantData extends LevelData 
{
	private ArrayList<GameFont> gameFont = new ArrayList<GameFont>();
	
	private String[] text = { "You have unlocked the color green! (???)" };
	
	
	public Level getLevel()
	{
		return new Level("/res/textures/Levels/levelPlant.png", 16);
	}

	public int[] getSpawnCoords() 
	{
		int[] coords = { 9080, 60 };
		return coords;
	}
	
	public LevelPlantData()
	{
		Player.unlockedBlue = true;
		Player.unlockedOrange = true;
		Player.unlockedPurple = true;
		Player.unlockedLBlue = true;
		Player.unlockedGreen = true;
		Player.totalUnlockedColors = 5;
		
		determineColors();
	}
	
	public void tick(Player player)
	{
		gameFont.clear();
		
		if(player.x > 8784 && player.x < 9088)
		{
			gameFont.add(new GameFont(text[0], (int)(Game.width/50), (int)(Game.height/3), 0xFF00A012, 0, 0, -1, true));
		}
		
	}
	
	public void render()
	{
		for(int i = 0; i < gameFont.size(); i++)
		{
			gameFont.get(i).render();
		}
	}

}
