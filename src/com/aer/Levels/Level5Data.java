package com.aer.Levels;

import java.util.ArrayList;

import com.aer.Entity.Player;
import com.aer.Main.Game;
import com.aer.Util.GameFont;

public class Level5Data extends LevelData
{
	private ArrayList<GameFont> gameFont = new ArrayList<GameFont>();
	
	private String[] text = { "You have unlocked the color blue!", "You can change the color of an", "editable tile by clicking on it!", "Right-clicking on a tile", "resets the tile", "Note: Not all tiles are editable.", "The strength of blue tiles increases", "when other blue tiles are touching them.", "Note: You can only place a certain", "number of tiles at a time, as", "noted by the top left corner."};
	
	
	public Level getLevel()
	{
		return new Level("/res/textures/Levels/Level5.png", 10);
	}

	public int[] getSpawnCoords() 
	{
		int[] coords = { 560, 455 };
		return coords;
	}
	
	public Level5Data()
	{
		if(!Player.hasCompletedExtraLevels)
		{
			Player.unlockedBlue = true;
			Player.unlockedOrange = false;
			Player.unlockedPurple = false;
			Player.unlockedLBlue = false;
			Player.unlockedGreen = false;
			
			Player.totalUnlockedColors = 1;
		}
		else
		{
			Player.unlockedBlue = true;
			Player.unlockedOrange = true;
			Player.unlockedPurple = true;
			Player.unlockedLBlue = true;
			Player.unlockedGreen = true;
			
			Player.totalUnlockedColors = 5;
		}
		
		determineColors();
	}
	
	public void tick(Player player)
	{
		gameFont.clear();
		
		if(player.x > 550 && player.x < 800)
		{
			gameFont.add(new GameFont(text[0], Game.width/2, (int)(Game.height/5.5), 0xFF0094FF, 0, 0, -1, true));
			gameFont.add(new GameFont(text[1], (int)(Game.width/1.95), (int)(Game.height/4.5), 0xFFFF0000, 0, 0, -1, true));
			gameFont.add(new GameFont(text[2], (int)(Game.width/1.94), (int)(Game.height/4.0), 0xFFFF0000, 0, 0, -1, true));
			gameFont.add(new GameFont(text[3], (int)(Game.width/1.94), (int)(Game.height/3.4), 0xFFFF0000, 0, 0, -1, true));
			gameFont.add(new GameFont(text[4], (int)(Game.width/1.84), (int)(Game.height/3.1), 0xFFFF0000, 0, 0, -1, true));
			gameFont.add(new GameFont(text[5], (int)(Game.width/1.94), (int)(Game.height/2.8), 0xFFA0A0A0, 0, 0, -1, true));
		}
		
		if(player.x > 800 && player.x < 980)
		{
			gameFont.add(new GameFont(text[6], (int)(Game.width/3.7), (int)(Game.height/5), 0xFFFF0000, 0, 0, -1, true));
			gameFont.add(new GameFont(text[7], (int)(Game.width/3.7), (int)(Game.height/4.5), 0xFFFF0000, 0, 0, -1, true));
		}
		
		if(player.x > 980 && player.x < 1182)
		{
			gameFont.add(new GameFont(text[8], (int)(Game.width/3.5), (int)(Game.height/5), 0xFFFF0000, 0, 0, -1, true));
			gameFont.add(new GameFont(text[9], (int)(Game.width/3.5), (int)(Game.height/4.5), 0xFFFF0000, 0, 0, -1, true));
			gameFont.add(new GameFont(text[10], (int)(Game.width/3.5), (int)(Game.height/4.0), 0xFFFF0000, 0, 0, -1, true));
		}
	}
	
	public void render()
	{
		for(int i = 0; i < gameFont.size(); i++)
		{
			gameFont.get(i).render();
		}
	}
}
