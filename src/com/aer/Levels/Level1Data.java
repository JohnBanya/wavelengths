package com.aer.Levels;

import java.util.ArrayList;

import com.aer.Entity.Player;
import com.aer.Main.Game;
import com.aer.Util.GameFont;

public class Level1Data extends LevelData
{
	private ArrayList<GameFont> gameFont = new ArrayList<GameFont>();
	
	private String[] text = { "Welcome to Operation Wavelenths.", "Somehow you, of all people, have made your way", "into our ultra-rigorous, secret test program.", "Seems like they let just about everybody in these days...", "Press the  A D  keys to walk.", "Press space to jump.", "Loudspeaker:" };
	
	private int timer_text1 = 280;
	private boolean text1_activated = true;
	
	private int timer_text2 = 280;
	private boolean text2_activated = false;
	
	private int timer_text3 = 280;
	private boolean text3_activated = false;
	
	public Level getLevel() 
	{
		return new Level("/res/textures/Levels/Level1.png", 0);
	}

	public int[] getSpawnCoords() 
	{
		int[] coords = { 192, 430 };
		return coords;
	}
	
	public Level1Data()
	{
		if(!Player.hasCompletedExtraLevels)
		{
			Player.unlockedBlue = false;
			Player.unlockedOrange = false;
			Player.unlockedPurple = false;
			Player.unlockedLBlue = false;
			Player.unlockedGreen = false;
			
			Player.totalUnlockedColors = 0;
		}
		else
		{	
			Player.unlockedBlue = true;
			Player.unlockedOrange = true;
			Player.unlockedPurple = true;
			Player.unlockedLBlue = true;
			Player.unlockedGreen = true;
			
			Player.totalUnlockedColors = 5;
		}
		
		determineColors();
	}
	
	public void tick(Player player)
	{
		gameFont.clear();
		tickTimers();
		
		if(text1_activated)
		{
			gameFont.add(new GameFont(text[0], Game.width/3, Game.height/8, 0xFF0026FF, 0, 0, -1, true));
			gameFont.add(new GameFont(text[6], Game.width/4, Game.height/11, 0xFF808080, 0, 0, -1, true));
		}
		
		if(text2_activated)
		{
			gameFont.add(new GameFont(text[1], Game.width/4, Game.height/8, 0xFF0026FF, 0, 0, -1, true));
			gameFont.add(new GameFont(text[2], Game.width/4, Game.height/6, 0xFF0026FF, 0, 0, -1, true));
		}
		
		if(text3_activated)
		{
			gameFont.add(new GameFont(text[3], Game.width/5, Game.height/8, 0xFF0026FF, 0, 0, -1, true));
		}
		
		
		if(player.x > 150 && player.x < 240)
		{
			gameFont.add(new GameFont(text[4], Game.width/2, (int)(Game.height/2.5), 0xFFFF0000, 0, 0, -1, true));
		}
		
		if(player.x > 900 && player.x < 1050)
		{
			gameFont.add(new GameFont(text[5], Game.width/2, (int)(Game.height/2.5), 0xFFFF0000, 0, 0, -1, true));
		}
	}
	
	private void tickTimers()
	{
		if(text1_activated)
		{
			if(timer_text1 > 0)
				timer_text1--;
			else
			{
				text1_activated = false;
				text2_activated = true;
			}
		}
		
		if(text2_activated)
		{
			if(timer_text2 > 0)
				timer_text2--;
			else
			{
				text2_activated = false;
				text3_activated = true;
			}
		}
		
		if(text3_activated)
		{
			if(timer_text3 > 0)
				timer_text3--;
			else
				text3_activated = false;
		}
	}
	
	public void render()
	{
		for(int i = 0; i < gameFont.size(); i++)
		{
			gameFont.get(i).render();
		}
	}
}
