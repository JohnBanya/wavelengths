package com.aer.Levels;

import java.util.ArrayList;

import com.aer.Entity.Player;
import com.aer.Util.GameFont;

public class Level24Data extends LevelData 
{
	private ArrayList<GameFont> gameFont = new ArrayList<GameFont>();
	
	private boolean timerActivated = true;
	private int timer = 1260;
	
	private boolean teleporterShutdown = false;
	
	
	public Level getLevel() 
	{
		return new Level("/res/textures/Levels/Level24.png", 10);
	}

	public int[] getSpawnCoords() 
	{
		int[] coords = { 110, 960 };
		return coords;
	}
	
	public Level24Data()
	{
		if(!Player.hasCompletedExtraLevels)
		{
			Player.unlockedBlue = true;
			Player.unlockedOrange = true;
			Player.unlockedPurple = true;
			Player.unlockedLBlue = true;
			Player.unlockedGreen = false;
			
			Player.totalUnlockedColors = 4;
		}
		else
		{
			Player.unlockedBlue = true;
			Player.unlockedOrange = true;
			Player.unlockedPurple = true;
			Player.unlockedLBlue = true;
			Player.unlockedGreen = true;
			
			Player.totalUnlockedColors = 5;
		}
		
		determineColors();
	}
	
	public void tick(Player player)
	{
		gameFont.clear();
		tickTimers();
		
		if(timerActivated)
		{
			if(((int)timer/60) > 0)
				gameFont.add(new GameFont("Teleporter shutdown in " + ((int)timer/60), 150, 13, 0xFFFF0000, 0, 0, -1, true));
			else
			{
				gameFont.add(new GameFont("Teleporter shutdown." , 150, 13, 0xFFFF0000, 0, 0, -1, true));
				teleporterShutdown = true;
			}
		}
	}
	
	private void tickTimers()
	{
		if(timer > 0 && timerActivated)
		{
			timer--;
		}
	}
	
	public void render()
	{
		for(int i = 0; i < gameFont.size(); i++)
		{
			gameFont.get(i).render();
		}
	}
	
	public boolean getTeleporterShutdown()
	{
		return teleporterShutdown;
	}
}
