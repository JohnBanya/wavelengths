package com.aer.Levels;

import java.util.ArrayList;

import com.aer.Entity.Player;
import com.aer.Main.Game;
import com.aer.Util.GameFont;

public class Level6Data extends LevelData
{
	private ArrayList<GameFont> gameFont = new ArrayList<GameFont>();
	
	private String[] text = { "You've been getting lucky so far.", "Lets make this more interesting!"};
	private boolean text1_activated = true;
	private int timer_text1 = 260;

	
	public Level getLevel()
	{
		return new Level("/res/textures/Levels/Level6.png", 4);
	}

	public int[] getSpawnCoords() 
	{
		int[] coords = { 76, 455 };
		return coords;
	}
	
	public Level6Data()
	{
		if(!Player.hasCompletedExtraLevels)
		{
			Player.unlockedBlue = true;
			Player.unlockedOrange = false;
			Player.unlockedPurple = false;
			Player.unlockedLBlue = false;
			Player.unlockedGreen = false;
			
			Player.totalUnlockedColors = 1;
		}
		else
		{
			Player.unlockedBlue = true;
			Player.unlockedOrange = true;
			Player.unlockedPurple = true;
			Player.unlockedLBlue = true;
			Player.unlockedGreen = true;
			
			Player.totalUnlockedColors = 5;
		}
		
		determineColors();
	}
	
	public void tick(Player player)
	{
		gameFont.clear();
		tickTimers();
		
		if(text1_activated)
		{
			gameFont.add(new GameFont(text[0],  (int)(Game.width/2.5), (int)(Game.height/5.5), 0xFF0026FF, 0, 0, -1, true));
			gameFont.add(new GameFont(text[1], (int)(Game.width/2.5), (int)(Game.height/4.5), 0xFF0026FF, 0, 0, -1, true));
		}
		
	}
	
	private void tickTimers()
	{
		if(text1_activated)
		{
			if(timer_text1 > 0)
				timer_text1--;
			else
			{
				text1_activated = false;
			}
		}
	}
	
	public void render()
	{
		for(int i = 0; i < gameFont.size(); i++)
		{
			gameFont.get(i).render();
		}
	}
}
