package com.aer.Levels;

import java.util.ArrayList;

import com.aer.Entity.Player;
import com.aer.Main.Game;
import com.aer.Util.GameFont;

public class Level13Data extends LevelData 
{
	private ArrayList<GameFont> gameFont = new ArrayList<GameFont>();
	
	private String[] text = { "You have unlocked the color purple!"};
	
	public Level getLevel() 
	{
		return new Level("/res/textures/Levels/Level13.png", 6);
	}

	public int[] getSpawnCoords() 
	{
		int[] coords = { 76, 115 };
		return coords;
	}
	
	public Level13Data()
	{
		if(!Player.hasCompletedExtraLevels)
		{
			Player.unlockedBlue = true;
			Player.unlockedOrange = true;
			Player.unlockedPurple = true;
			Player.unlockedLBlue = false;
			Player.unlockedGreen = false;
			
			Player.totalUnlockedColors = 3;
		}
		else
		{
			Player.unlockedBlue = true;
			Player.unlockedOrange = true;
			Player.unlockedPurple = true;
			Player.unlockedLBlue = true;
			Player.unlockedGreen = true;
			
			Player.totalUnlockedColors = 5;
		}
		
		
		determineColors();
	}
	
	public void tick(Player player)
	{
		gameFont.clear();
		
		if(player.x > 27 && player.x < 500 && player.y < 130 && player.y > 0)
		{
			gameFont.add(new GameFont(text[0], (int)(Game.width/2.3), (int)(Game.height/3), 0xFF8800CC, 0, 0, -1, true));
		}
	}
	
	public void render()
	{
		for(int i = 0; i < gameFont.size(); i++)
		{
			gameFont.get(i).render();
		}
	}

}
