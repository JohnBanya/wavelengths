package com.aer.Levels;

import java.util.ArrayList;

import com.aer.Entity.Player;
import com.aer.Main.Game;
import com.aer.Util.GameFont;

public class Level30Data extends LevelData 
{
	private ArrayList<GameFont> gameFont = new ArrayList<GameFont>();
	
	private String[] text = { "Do not go through that exit. Think about all the consequences.", "All the years of work and dedication, gone.", "You're family can survive without you.", "Please turn back now. Its not too late."};
	
	private int timer_text1 = 360;
	private boolean text1_activated = true;
	
	private int timer_text2 = 360;
	private boolean text2_activated = false;
	
	
	public Level getLevel()
	{
		return new Level("/res/textures/Levels/Level30.png", 0);
	}

	public int[] getSpawnCoords() 
	{
		int[] coords = { 314, 430 };
		return coords;
	}
	
	public Level30Data()
	{
		if(!Player.hasCompletedExtraLevels)
		{
			Player.unlockedBlue = true;
			Player.unlockedOrange = true;
			Player.unlockedPurple = true;
			Player.unlockedLBlue = true;
			Player.unlockedGreen = false;
			
			Player.totalUnlockedColors = 4;
		}
		else
		{
			Player.unlockedBlue = true;
			Player.unlockedOrange = true;
			Player.unlockedPurple = true;
			Player.unlockedLBlue = true;
			Player.unlockedGreen = true;
			
			Player.totalUnlockedColors = 5;
		}
		
		determineColors();
	}
	
	public void tick(Player player)
	{
		gameFont.clear();
		tickTimers();

		if(text1_activated)
		{
			gameFont.add(new GameFont(text[0], Game.width/5, Game.height/8, 0xFF0026FF, 0, 0, -1, true));
			gameFont.add(new GameFont(text[1], (int)(Game.width/4), (int)(Game.height/6.5), 0xFF0026FF, 0, 0, -1, true));
		}
	
		if(text2_activated)
		{
			gameFont.add(new GameFont(text[2], Game.width/5, Game.height/8, 0xFF0026FF, 0, 0, -1, true));
			gameFont.add(new GameFont(text[3], (int)(Game.width/4), (int)(Game.height/6.5), 0xFF0026FF, 0, 0, -1, true));
		}
	}
	
	private void tickTimers()
	{
		if(text1_activated)
		{
			if(timer_text1 > 0)
				timer_text1--;
			else
			{
				text1_activated = false;
				text2_activated = true;
			}
		}
		
		if(text2_activated)
		{
			if(timer_text2 > 0)
				timer_text2--;
			else
			{
				text2_activated = false;
			}
		}
	}
	
	public void render()
	{
		for(int i = 0; i < gameFont.size(); i++)
		{
			gameFont.get(i).render();
		}
	}

}
