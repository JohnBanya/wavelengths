package com.aer.Tiles;

import com.aer.Main.Game;
import com.aer.Textures.Sprite;

public class Blank extends Tile 
{
	public Blank(Sprite sprite) 
	{
		super(sprite);
	}

	public void render(int xt, int yt)
	{
		int x = xt * size;
		int y = yt * size;
		Game.screen.render(x, y, sprite);
	}
	
	public int setColor(int col)
	{
		return 0;
	}
}
