package com.aer.Tiles;

import com.aer.Main.Game;
import com.aer.Textures.Sprite;

public class Editable extends Tile
{
	public Editable(Sprite sprite){
		super(sprite);
	}

	public void render(int xt, int yt) 
	{
		int x = xt * size;
		int y = yt * size;
		
		Game.screen.renderSwapColor(x, y, sprite, 0xFFBC00A6, col);
	}
	
	public TileData getTileData()
	{
		return new TileData(true, col, false);
	}

	public int setColor(int col)
	{
		if(this.col == col)
		{
			return 0;
		}
		else if(this.col == 0xFFFFFFFF)
		{
			this.col = col;
			return 1;
		}
		else if(this.col != 0xFFFFFFFF && col != 0xFFFFFFFF)
		{
			this.col = col;
			return 0;
		}
		else if(col == 0xFFFFFFFF)
		{
			this.col = col;
			return -1;
		}
		else
		{
			return 0;
		}
	}
}
