package com.aer.Tiles;

import com.aer.Main.Game;
import com.aer.Textures.AnimatedSprite;

public class LocalTeleportTile extends Tile
{
	private AnimatedSprite anim;
	
	public LocalTeleportTile(AnimatedSprite anim, int col)
	{
		super(anim.getSprite());
		this.col = col;
		this.anim = anim;
	}
	
	public void render(int xt, int yt) 
	{
		int x = xt * size;
		int y = yt * size;
		
		Game.screen.renderSwapColor(x, y, anim.getSprite(), 0xFFBC00A6, col);
	}
	
	public void tick()
	{
		anim.tick();
	}
	
	public TileData getTileData()
	{
		return new TileData(false, col, true);
	}
	
	public int setColor(int col)
	{
		return 0;
	}
}
