package com.aer.Tiles;

public class TileData 
{
	public final boolean SOLID;
	public final int COLOR;
	public final boolean TELEPORTER;
	
	public TileData(boolean solid, int col, boolean teleporter)
	{
		SOLID = solid;
		COLOR = col;
		TELEPORTER = teleporter;
	}
}
