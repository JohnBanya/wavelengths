package com.aer.Tiles;

import com.aer.Main.Game;
import com.aer.Textures.Sprite;

public class SolidTile extends Tile 
{
	protected int col = 0xFFFFFFFF;
	
	public SolidTile(Sprite sprite, int color)
	{
		super(sprite);
		col = color;
	}

	public void render(int xt, int yt) 
	{
		int x = xt * size;
		int y = yt * size;
		
		Game.screen.renderSwapColor(x, y, sprite, 0xFFBC00A6, col);
	}

	public TileData getTileData()
	{
		return new TileData(true, col, false);
	}
	
	public int setColor(int col)
	{
		return 0;
	}
}
