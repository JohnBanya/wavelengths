package com.aer.Tiles;

import com.aer.Textures.Sprite;

public abstract class Tile 
{
	protected int col = 0xFFFFFFFF;

	protected Sprite sprite;
	public int size;
	
	//Tile Colors
	public static final int COL_EDITABLE = 0xFF808080;
	public static final int COL_SOLID = 0xFF000000;
	public static final int COL_SOLID_BLUE = 0xFF00355B;
	public static final int COL_SOLID_ORANGE = 0xFF823400;
	public static final int COL_SOLID_RED = 0xFF7F0000;
	public static final int COL_SOLID_PURPLE = 0xFF4B1172;
	public static final int COL_BLANK_EMPTY_WINDOW = 0xFFFFD800;
	public static final int COL_BLANK_PERSON_WINDOW = 0xFFE5BF00; 
	public static final int COL_BLANK_PERSON_EMPTY_WINDOW = 0xFFF7CE00;
	public static final int COL_PLANT = 0xFF00CE18;
	public static final int COL_PLANT_WINDOW = 0xFFB6FF00;
	public static final int COL_SOLID_GREEN = 0xFF004908;
	public static final int COL_TELEPORT = 0xFF00FFFF;
	
	public Tile(Sprite sprite)
	{
		this.sprite = sprite;
		size = sprite.SIZE;
	}
	
	public abstract void render(int xt, int yt);
	
	public void tick()
	{
		
	}
	
	public TileData getTileData()
	{
		return new TileData(false, col, false);
	}
	
	public int setColor(int col)
	{
		this.col = col;
		
		return 1;
	}
}
