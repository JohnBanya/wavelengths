package com.aer.Tiles;

import com.aer.Main.Game;
import com.aer.Textures.AnimatedSprite;
import com.aer.Textures.Sprite;

public class TeleportTile extends Tile
{
	private AnimatedSprite anim;
	
	public TeleportTile(Sprite sprite)
	{
		super(sprite);
	}
	
	public TeleportTile(AnimatedSprite anim)
	{
		super(anim.getSprite());
		this.anim = anim;
	}

	public void render(int xt, int yt) 
	{
		int x = xt * size;
		int y = yt * size;
		
		if(anim == null)
			Game.screen.render(x, y, sprite);
		else
			Game.screen.render(x, y, anim.getSprite());
	}
	
	public void tick()
	{
		if(anim != null)
		{
			anim.tick();
		}
	}
	
	public TileData getTileData()
	{
		return new TileData(false, 0xFFFF7F7F, true); // 0xFFFF7F7F is the color checked for inter-level teleportation
	}
	
	public int setColor(int col)
	{
		return 0;
	}
}
