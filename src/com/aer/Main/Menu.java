package com.aer.Main;

import java.util.ArrayList;

import com.aer.Entity.Player;
import com.aer.Textures.Sprite;
import com.aer.Textures.SpriteSheet;
import com.aer.Util.GameFont;

public class Menu 
{
	private ArrayList<GameFont> levelGameFont = new ArrayList<GameFont>(); // holds the text for the levels (level names, unlocked, etc...)
	
	private boolean startLevel = false;
	private int startLevelAtValue = 1;
	
	private boolean loadScreenOpened = false;
	private boolean hasPrintedLevels = false;
	
	private boolean saveActivated = false;
	private boolean loadActivated = false;
	
	private int timer = 0;
	
	private boolean activated = false;		// Keep track of whether the menu is open or not
	
	public void tick()
	{	
		if(timer > 0)
			timer--;
		
		if(loadScreenOpened && !hasPrintedLevels)
		{
			tickLoadScreen();
		}
		
		if(Mouse.getButton() == 1) 
		{
			tickMouseClick();
		} 
		
	}
	
	public void render()
	{
		new GameFont("v" + Game.versionNumber, 5, 5, 0xFFA0A0A0, 0, 0, -1, true).render();
		
		if(!loadScreenOpened)
		{
			// Main Screen
			Game.screen.renderIgnoreOffsets(125, 0, SpriteSheet.title);
			Game.screen.renderIgnoreOffsets(195, 120, SpriteSheet.start_button);
			Game.screen.renderIgnoreOffsets(195, 170, SpriteSheet.load_button);
			new GameFont("Save Game", 425, 260, 0xFFA0A0A0, 0, 0, -1, true).render();
		}
		else
		{
			//Load screen
			for(int i = 0; i < levelGameFont.size(); i++)
			{
				GameFont g = levelGameFont.get(i);
				g.render();
				
				// Star rendering
				if(Player.unlockedLevel[i])
				{
					int star_offset = 5;
					for(int j = 0; j < 5; j++)
					{
						if(j < Player.levelRating[i])
						{
							Game.screen.renderIgnoreOffsets(g.getX() + g.getWidth() + star_offset, g.getY(), Sprite.star);
						}
						else
						{
							Game.screen.renderIgnoreOffsets(g.getX() + g.getWidth() + star_offset, g.getY(), Sprite.star_empty);
						}
						star_offset += 7;
					}
				}
			}
			
			new GameFont("Load Game File", 400, 260, 0xFFA0A0A0, 0, 0, -1, true).render();
			Game.screen.renderIgnoreOffsets(10, 250, SpriteSheet.arrow_left);
		}
	}
	
	private void tickLoadScreen()
	{
		int yOffset = 15;
		int xOffset = 100;
		
		for(int i = 0; i < Player.unlockedLevel.length; i++)
		{
			int unlockedColor = 0xFFFF0000;
			String text = "Level " + (i+1) + " ";
			
			if(Player.unlockedLevel[i])
			{
				text += "(UNLOCKED)";
				
				unlockedColor = 0xFF00E01A;
			}
			else
				text += "(LOCKED)";
			
			levelGameFont.add(new GameFont(text, xOffset, yOffset, unlockedColor, 0, 0, -1, true));
			
			yOffset += 15;
			
			if(yOffset > 250)
			{
				xOffset += 190;
				yOffset = 15;
			}
		}
		
		hasPrintedLevels = true;
	}
	
	private void tickMouseClick()
	{
		if(!loadScreenOpened)
		{
			// Main screen
			if(Mouse.getScaledX() > 195 && Mouse.getScaledX() < 318 && Mouse.getScaledY() > 119 && Mouse.getScaledY() < 157) // Start button hitbox
			{
				startLevel = true;
				startLevelAtValue = 1;
			}
			else if(Mouse.getScaledX() > 195 && Mouse.getScaledX() < 318 && Mouse.getScaledY() > 169 && Mouse.getScaledY() < 208) // Load button hitbox
			{
				levelGameFont.clear();
				hasPrintedLevels = false;
				loadScreenOpened = true;
				timer = 20; // prevents clicking the button once and hitting something at the next screen (as it loads faster than the user can release the mouse button)
			}
			else if(Mouse.getScaledX() > 425 && Mouse.getScaledX() < 480 && Mouse.getScaledY() > 260 && Mouse.getScaledY() < 266) // Save button
			{
				saveActivated = true;
			}
		}
		else if(timer <= 0)
		{
			//When loading a level
		
			// Back arrow
			if(Mouse.getScaledX() > 9 && Mouse.getScaledX() < 29 && Mouse.getScaledY() > 249 && Mouse.getScaledY() < 269) 
			{
				levelGameFont.clear();
				hasPrintedLevels = false;
				loadScreenOpened = false;
			}
			
			
			// Load Level text
			int loadedLevel = -1;
			int yOffset = 15;
			int xOffset = 100;
			
			for(int i = 0; i < Player.unlockedLevel.length; i++) // Level word click
			{
				String text = "Level " + (i+1) + " ";
				
				if(Player.unlockedLevel[i])
					text += "(UNLOCKED)";
				else
					text += "(LOCKED)";
				
				
				if(Mouse.getScaledX() >= xOffset && Mouse.getScaledX() <= (xOffset+(text.length() * 6)) && Mouse.getScaledY() >= yOffset && Mouse.getScaledY() <= (yOffset + 6)) // Individual text
				{
					if(Player.unlockedLevel[i])
						loadedLevel = i+1;
				}
				
				
				yOffset += 15;
				
				if(yOffset > 250)
				{
					xOffset += 190;
					yOffset = 15;
				}
			}
			
			if(Mouse.getScaledX() > 400 && Mouse.getScaledX() < 484 && Mouse.getScaledY() > 260 && Mouse.getScaledY() < 266) // Save button
			{
				loadActivated = true;
				loadScreenOpened = false;
			}
			
			//Check load level text
			if(loadedLevel != -1)
			{
				levelGameFont.clear();
				hasPrintedLevels = false;
				loadScreenOpened = false;
				
				startLevel = true;
				startLevelAtValue = loadedLevel;
			}

		}
	}
	
	public int getStartLevel()
	{
		int level = -1;
		
		if(startLevel)
		{
			level = startLevelAtValue;
		}
		
		return level;
	}
	
	public boolean getSaveActivated()
	{
		return saveActivated;
	}
	
	public void resetSaveActivated()
	{
		saveActivated = false;
	}

	public boolean getLoadActivated()
	{
		return loadActivated;
	}
	
	public void resetLoadActivated()
	{
		loadActivated = false;
	}
	
	public void setActivated(boolean a)
	{
		if(a == false)
		{
			saveActivated = false;
			loadActivated = false;
			startLevel = false;
			startLevelAtValue = -1;
			loadScreenOpened = false;
			hasPrintedLevels = false;
			timer = 0;
		}
		
		activated = a;
	}
	
	public boolean getActivated()
	{
		return activated;
	}
}
