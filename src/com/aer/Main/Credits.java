package com.aer.Main;

import java.util.ArrayList;

import com.aer.Util.GameFont;

public class Credits 
{
	private ArrayList<GameFont> gameFont = new ArrayList<GameFont>();
	
	private final int TIMER_VALUE = 280;
	private int timer = 280;
	private boolean escaped = false;
	
	private boolean activated = false;
	
	
	public Credits(boolean escaped)
	{
		this.escaped = escaped;
	}
	
	public void tick()
	{
		gameFont.clear();
		tickTimers();
		
		if(escaped) // Second possible credits ending (escaped)
		{
			if(timer < 220 && timer > 0)
			{
				gameFont.add(new GameFont("Farewell, Subject 147.", (int)(Game.width/2.5), Game.height/3, 0xFFFFFFFF, 0, 0, -1, true));
			}
			else if(timer <= 0) // Reset credits
			{
				escaped = false;
				activated = false;
				timer = TIMER_VALUE;
			}
		}
		else // First possible credits ending (captured)
		{
			if(timer < 220 && timer > 0)
			{
				gameFont.add(new GameFont("Thank you for efforts toward scientific progress.", (int)(Game.width/4.5), Game.height/3, 0xFFFFFFFF, 0, 0, -1, true));
			}
			else if(timer <= 0) // Reset credits
			{
				escaped = false;
				activated = false;
				timer = TIMER_VALUE;
			}
		}
		
	}
	
	
	public void setEscaped(boolean e)
	{
		escaped = e;
	}
	
	public boolean getEscaped()
	{
		return escaped;
	}
	
	
	private void tickTimers()
	{
		if(timer > 0)
			timer--;
	}
	
	
	public void render()
	{
		for(int i = 0; i < gameFont.size(); i++)
		{
			gameFont.get(i).render();
		}
	}
	
	
	public void setActivated(boolean a)
	{
		if(a == false)
			timer = TIMER_VALUE;
		
		activated = a;
	}
	
	public boolean getActivated()
	{
		return activated;
	}
}
