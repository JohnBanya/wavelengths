package com.aer.Main;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.UIManager;

import com.aer.Entity.Player;
import com.aer.Levels.LevelLoader;
import com.aer.Util.SaveFile;

public class Game extends Canvas implements Runnable
{
	private static final long serialVersionUID = 1L;
	public static final String versionNumber = "1.6.1";		// Game version
	public static int width = 500;		// Width of game (non-scaled)
	public static int height = width / 16 * 9;		// Height of game (non-scaled) relative to width
	public static int scale = 2;		// Scale of game (stretch game pixels)
	public static String title = "Wavelengths";		// Game title
	
	public static Screen screen;		// Only one instance of screen is needed (there can only be one screen). Making this static makes it significantly easier to use in the code
	private JFrame frame;		// The JFrame that holds the game
	private Thread thread;		// The main thread that runs the game
	private Keyboard key;		// The main Keyboard from which key actions are read from
	private Mouse mouse;		// The main Mouse from which mouse actions are read from
	private LevelLoader levelLoader;		// Object used to load level data
	
	private boolean running = false;		// Game running (true/false)
	
	private BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);			// The image that is displayed to the screen every frame (constantly updated)
	
	// Link to the BufferedImage image raster to the int[] array so that changes to the array results in changes to the image
	// A raster is a data structure that is generally thought of as a grid of pixels	
	private int[] pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();		
	
	public static Menu menu;		// Main menu object
	public static Credits credits;		// Main Credits object
	
	public double xScroll, yScroll; 	// Used in setting screen x/y
	
	
	public Game() // Constructor (run before main)
	{
		Dimension size = new Dimension(width*scale, height*scale);		// Create a Dimension object that uses scaled width and height
		setPreferredSize(size);		// Set the size of the canvas
		frame = new JFrame();	
		screen = new Screen(width, height);		// Create a Screen object of width and height
		
		// Create a keyboard object and attach a key listener to it
		key = new Keyboard();
		addKeyListener(key);	
		
		// Create a mouse object and attach action listeners to it
		mouse = new Mouse();
		addMouseListener(mouse);
		addMouseMotionListener(mouse);
		addMouseWheelListener(mouse);
		
		// Create a menu
		menu = new Menu();
		menu.setActivated(true); // Activate the menu
		
		credits = new Credits(false);
		credits.setActivated(false);
	}
	
	public static void main(String[] args) 
	{
		// Create the game
		Game game = new Game();
		
		game.frame.setTitle(title + " v" + versionNumber);		// Set the Game title with the name and the version
		game.frame.setResizable(false);	// Make the frame non-resizable
		game.frame.add(game);		// Add the canvas to the frame
		game.frame.pack();		// Fit the frame to the inner components
		game.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);		// Set game to close when the close button is clicked
		game.frame.setLocation(150, 150);		// Set start-location of game on screen (relative to top-left of screen)
		game.frame.setVisible(true);		// Make game visible
		
		try 
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());		// Set the frame to fit the System's default look-and-feel (Changes file-loading screen)
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		// Call the start method to begin the game
		game.start();
	}
	
	public void start()
	{
		// Set the game to run and begin the game thread
		running = true;
		thread = new Thread(this);
		thread.start(); // Calls the thread's run method (see: public void run() )
	}
	
	public void run()
	{
		// This method is the locations of the game's main timer/loop and is constantly run as long as the game is open
		
		requestFocus();		// Set the window to main focus
		
		// Declare variables to keep track of the ticks per second and frames per second
		int tickCount = 0;
		int frames = 0;
		
		// Declare variables that will be used to keep the game speed consistent:
		long startTime = System.nanoTime();		// Get time at start of program
		long timer = System.currentTimeMillis();	// Timer to keep track of frames/ticks per second
		double delta = 0;		// Used to keep track of differences in time between loops
		final double conversionFactor = 1000000000.0 / 60.0;		// Conversion number:  1 x 10^9 nanoseconds (1 second) = 60 in-game ticks (60 ticks per second)
		
		while(running)
		{
			// Keep track of game time to make ticks (game speed) consistent:
			long now = System.nanoTime();		// Get current system time
			delta = delta + (now - startTime) / conversionFactor;		// Subtract the current time and the start time (time since last loop), convert to ticks, then add to delta
			startTime = System.nanoTime();		// set startTime to current time
			
			while(delta > 1) // if delta is greater than 1, a game tick is due
			{
				tick();		// Call the game tick method when a tick is due (handles time-sensitive events)
				delta--;
				tickCount++;		// Used to keep track of ticks per second (tps)
			}
			
			render();		// Render runs as fast as possible (every loop, unlike tick() ). Handles things that are not time sensitive, such as frames per second (fps)
			frames++; 		// Used to keep track of frame per second (fps)
			
			
			// Update the frames/ticks per second (fps/tps) every one second, then reset the frames and tickCount variables
			if(System.currentTimeMillis() - timer >= 1000)
			{
				timer = System.currentTimeMillis();
				
				frame.setTitle(title + " v" + versionNumber + " | " + tickCount + " tps, " + frames + " fps"); 		// Update title to include current fps and tps
				
				// Reset variables that keep track of tps/fps
				tickCount = 0;
				frames = 0;
			}
			
			
			//Limit fps (less strain on computer) by creating delays between loops:
			/*
			try
			{
				Thread.sleep(1);
			}
			catch(Exception e)
			{
				
			}
			*/
			
		}
	}
	
	public void tick()
	{
		// Handles time-sensitive events (runs 60 times per second)
		
		if(credits.getActivated() == false && menu.getActivated() == false) // ticks level (both credits and menu are inactive)
		{
			key.tick();
			levelLoader.tick();
		}
		else if(credits.getActivated()) // credits are activated
		{
			credits.tick();
			
			if(credits.getActivated() == false)
			{
				menu.setActivated(true);
			}
		}
		else // menu
		{
			menu.tick();
			
			if(menu.getStartLevel() != -1) // A level is selected to be loaded
			{
				levelLoader = new LevelLoader(key, menu.getStartLevel());	// Load the selected level
				menu.setActivated(false);
				credits.setActivated(false);
			}
			else if(menu.getSaveActivated()) // Save game
			{
				menu.resetSaveActivated();
				
				JFileChooser fileSave = new JFileChooser();
				fileSave.showSaveDialog(frame);
				File userFile = fileSave.getSelectedFile();
				
				try
				{
					FileOutputStream fileStream = new FileOutputStream(userFile);
					ObjectOutputStream os = new ObjectOutputStream(fileStream);
					os.writeObject(new SaveFile(Player.unlockedLevel, Player.levelRating, Player.hasCompletedExtraLevels));
					os.close();
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
			else if(menu.getLoadActivated()) // Load game
			{
				menu.resetLoadActivated();
				
				JFileChooser fileOpen = new JFileChooser();
				fileOpen.showOpenDialog(frame);
				File userFile = fileOpen.getSelectedFile();
				
				try
				{ 
					FileInputStream fileIn = new FileInputStream(userFile);
					ObjectInputStream is = new ObjectInputStream(fileIn);
					SaveFile sf = (SaveFile) is.readObject();	
					is.close();
					
					Player.unlockedLevel = sf.getUnlockedLevels();
					Player.levelRating = sf.getLevelRating();
					Player.hasCompletedExtraLevels = sf.getHasCompletedExtraLevels();
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}

		} // end menu
		
	}
	
	public void render()
	{
		// Handles screen events (rendering). Runs as many times as possible per second (varies from computer to computer)
		
		BufferStrategy bs = getBufferStrategy(); // Get the current buffer strategy (display mechanism) from canvas
		
		if(bs == null)	// If no current buffer strategy (usually first time loading)
		{
			createBufferStrategy(3); // Triple buffer (multi-buffering for better performance)
			return;
		}
		
		screen.clear(); // Clear the screen object
		
		// Handle menu/credit screens
		if(credits.getActivated() == false && menu.getActivated() == false) 		// Credits and menu inactive, render game
		{
			// Set the screen x/y offsets based on player x/y coordinates
			xScroll = levelLoader.getPlayerX() - (width/2 - 16); 
			yScroll = levelLoader.getPlayerY() - (height/2 - 16);
			
			Game.screen.setOffset((int)xScroll, (int)yScroll);
			
			levelLoader.render();	// Render the levelLoader (which calls further render methods)
		}
		else if(credits.getActivated())		// Render credits
		{
			credits.render();
		}
		else		// Render menu
		{	
			menu.render();
		}
		
	
		// Copy pixels from screen
		int[] p = screen.getPixels();
		
		// Transfer pixels from screen to the Game pixels (what the user sees)
		for(int i = 0; i < pixels.length; i++)
		{
			pixels[i] = p[i];
		}
		
		
		Graphics g = bs.getDrawGraphics(); // Get a Graphics object to draw to screen
		
		// Draw the created image to screen
		g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
		
		// Release system resources from graphics rendering
		g.dispose();
		bs.show();
	}
}
