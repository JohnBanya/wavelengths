package com.aer.Main;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener
{
	private boolean[] keys = new boolean[120];
	public boolean up, down, left, right, space;	
	public boolean key1, key2, key3, key4, key5, key6, key7, key8, key9, key0;
	public boolean key_r, key_esc;
	
	public void tick()
	{
		// Apply specific key booleans to true/false based on the value in the array
		up = keys[KeyEvent.VK_UP] || keys[KeyEvent.VK_W];
		down = keys[KeyEvent.VK_DOWN] || keys[KeyEvent.VK_S];
		left = keys[KeyEvent.VK_LEFT] || keys[KeyEvent.VK_A];
		right = keys[KeyEvent.VK_RIGHT] || keys[KeyEvent.VK_D];
		space = keys[KeyEvent.VK_SPACE];
		
		key1 = keys[KeyEvent.VK_1];
		key2 = keys[KeyEvent.VK_2];
		key3 = keys[KeyEvent.VK_3];
		key4 = keys[KeyEvent.VK_4];
		key5 = keys[KeyEvent.VK_5];
		key6 = keys[KeyEvent.VK_6];
		key7 = keys[KeyEvent.VK_7];
		key8 = keys[KeyEvent.VK_8];
		key9 = keys[KeyEvent.VK_9];
		key0 = keys[KeyEvent.VK_0];
		
		key_r = keys[KeyEvent.VK_R];
		key_esc = keys[KeyEvent.VK_ESCAPE];
	}
	
	public void keyPressed(KeyEvent e) 
	{
		// When a key is pressed, the code for the key (a number) is used to set a boolean to true
		keys[e.getKeyCode()] = true;
	}


	public void keyReleased(KeyEvent e) 
	{
		// When a key is released, the code for the key (a number) is used to set a boolean to false
		keys[e.getKeyCode()] = false;
	}


	public void keyTyped(KeyEvent e) 
	{
		
	}
}
