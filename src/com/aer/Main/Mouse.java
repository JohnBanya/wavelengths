package com.aer.Main;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

public class Mouse implements MouseListener, MouseMotionListener, MouseWheelListener
{
	private static int mouseX = -1;
	private static int mouseY = -1;
	private static int mouseB = -1;
	private static int mouseWheelMotion = 0;
	
	public static int getX()
	{
		return mouseX;
	}
	
	public static int getY()
	{
		return mouseY;
	}
	
	public static int getMouseWheelMotion()
	{
		int m = mouseWheelMotion;
		mouseWheelMotion = 0;
		
		return m;
	}
	
	public static int getScaledX()
	{
		return (int)(mouseX / Game.scale);
	}
	
	public static int getScaledY()
	{
		return (int)(mouseY / Game.scale);
	}
	
	public static int getButton()
	{
		return mouseB;
	}
	
	public void mouseDragged(MouseEvent e) 
	{
		mouseX = e.getX();
		mouseY = e.getY();
	}

	public void mouseMoved(MouseEvent e) 
	{
		mouseX = e.getX();
		mouseY = e.getY();
	}

	public void mouseClicked(MouseEvent e) 
	{
	
	}

	public void mouseEntered(MouseEvent e) 
	{
		
	}

	public void mouseExited(MouseEvent e) 
	{
		
	}
	
	public void mousePressed(MouseEvent e) 
	{
		mouseB = e.getButton();
	}

	public void mouseReleased(MouseEvent e) 
	{
		mouseB = -1;
	}

	
	public void mouseWheelMoved(MouseWheelEvent e)
	{
		mouseWheelMotion = e.getWheelRotation(); // mouseWheelMotion = -1 is UP, mouseWheelMotion = 1 is DOWN
	}
	
}
