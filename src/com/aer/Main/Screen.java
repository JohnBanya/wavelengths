package com.aer.Main;

import java.awt.Color;
import com.aer.Textures.Sprite;
import com.aer.Textures.SpriteSheet;

public class Screen 
{
	private int width, height;
	private int xOffset, yOffset;
	private int[] pixels;
	
	private int backgroundColor = 0;
	
	public Screen(int width, int height)
	{
		this.width = width;
		this.height = height;
		pixels = new int[width*height];
	}
	
	public synchronized void render(int x, int y, Sprite sprite)
	{
		// Normal render method
		
		int xp = x - xOffset;
		int yp = y - yOffset;
		
		int[] px = sprite.getPixels();
		
		for(int yl = 0; yl < sprite.HEIGHT; yl++)
		{
			int ya = yl + yp;
			for(int xl = 0; xl < sprite.WIDTH; xl++)
			{
				int xa = xl + xp;
				
				if(!(xa < 0 || xa >= width || ya < 0 || ya >= height))
				{
					int col = px[xl+yl*sprite.WIDTH];
					
					if(col != 0xFFFF00DE)
						pixels[xa+ya*width] = col;
				}
			}
		}
	}
	
	public synchronized void renderIgnoreOffsetsRectangle(int x, int y, int w, int h, int color)
	{
		// Manually draw a rectangle without using a picture file
		
		int xp = x;
		int yp = y;
		
		for(int yl = 0; yl < h; yl++)
		{
			int ya = yl + yp;
			for(int xl = 0; xl < w; xl++)
			{
				int xa = xl + xp;
				
				if(!(xa < -w || xa >= width || ya < 0 || ya >= height))
				{
					if(xa < 0) 
						xa = 0;
					
					pixels[xa+ya*width] = color;
				}
			}
		}
	}
	
	public synchronized void renderIgnoreOffsetsSwapColor(int x, int y, Sprite sprite, int colorToSwap, int newColor)
	{
		// Normal render method
		
		int xp = x;
		int yp = y;
		
		int[] px = sprite.getPixels();
		
		for(int yl = 0; yl < sprite.HEIGHT; yl++)
		{
			int ya = yl + yp;
			for(int xl = 0; xl < sprite.WIDTH; xl++)
			{
				int xa = xl + xp;
				
				if(!(xa < -sprite.WIDTH || xa >= width || ya < 0 || ya >= height))
				{
					if(xa < 0) 
						xa = 0;
					
					int col = px[xl+yl*sprite.WIDTH];
					
					if(col != 0xFFFF00DE)
					{
						if(col == colorToSwap)
							pixels[xa+ya*width] = newColor;
						else
							pixels[xa+ya*width] = col;
					}
				}
			}
		}
	}
	
	public synchronized void renderSwapColor(int x, int y, Sprite sprite, int colorToSwap, int newColor)
	{
		// Normal render method
		
		int xp = x - xOffset;
		int yp = y - yOffset;
		
		int[] px = sprite.getPixels();
		
		for(int yl = 0; yl < sprite.HEIGHT; yl++)
		{
			int ya = yl + yp;
			for(int xl = 0; xl < sprite.WIDTH; xl++)
			{
				int xa = xl + xp;
				
				if(!(xa < -sprite.WIDTH || xa >= width || ya < 0 || ya >= height))
				{
					if(xa < 0) 
						xa = 0;
					
					int col = px[xl+yl*sprite.WIDTH];
					
					if(col != 0xFFFF00DE)
					{
						if(col == colorToSwap)
							pixels[xa+ya*width] = newColor;
						else
							pixels[xa+ya*width] = col;
					}
				}
			}
		}
	}
	
	public synchronized void renderSwapColor(int x, int y, Sprite sprite, int colorToSwap, int newColor, int colorToSwap2, int newColor2)
	{
		// Normal render method
		
		int xp = x - xOffset;
		int yp = y - yOffset;
		
		int[] px = sprite.getPixels();
		
		for(int yl = 0; yl < sprite.HEIGHT; yl++)
		{
			int ya = yl + yp;
			for(int xl = 0; xl < sprite.WIDTH; xl++)
			{
				int xa = xl + xp;
				
				if(!(xa < -sprite.WIDTH || xa >= width || ya < 0 || ya >= height))
				{
					if(xa < 0) 
						xa = 0;
					
					int col = px[xl+yl*sprite.WIDTH];
					
					if(col != 0xFFFF00DE)
					{
						if(col == colorToSwap)
							col = newColor;
						else if (col == colorToSwap2)
							col = newColor2;
						
							pixels[xa+ya*width] = col;
					}
				}
			}
		}
	}
	
	public synchronized void renderIgnoreOffsets(int x, int y, Sprite sprite)
	{
		// Render method that ignores world offsets (always stays in location relative to screen)
		
		int xp = x;
		int yp = y;
		
		int[] px = sprite.getPixels();
		
		for(int yl = 0; yl < sprite.HEIGHT; yl++)
		{
			int ya = yl + yp;
			for(int xl = 0; xl < sprite.WIDTH; xl++)
			{
				int xa = xl + xp;
				
				if(!(xa < -sprite.WIDTH || xa >= width || ya < 0 || ya >= height))
				{
					if(xa < 0) 
						xa = 0;
					
					int col = px[xl+yl*sprite.WIDTH];
					
					if(col != 0xFFFF00DE)
						pixels[xa+ya*width] = col;
				}
			}
		}
	}
	
	public synchronized void renderIgnoreOffsets(int x, int y, SpriteSheet sprite)
	{
		// Normal render method
		
		int xp = x;
		int yp = y;
		
		int[] px = sprite.getPixels();
		
		for(int yl = 0; yl < sprite.getHeight(); yl++)
		{
			int ya = yl + yp;
			for(int xl = 0; xl < sprite.getWidth(); xl++)
			{
				int xa = xl + xp;
				
				if(!(xa < -sprite.getWidth() || xa >= width || ya < 0 || ya >= height))
				{
					if(xa < 0) 
						xa = 0;
					
					int col = px[xl+yl*sprite.getWidth()];
					
					if(col != 0xFFFF00DE)
						pixels[xa+ya*width] = col;
				}
			}
		}
	}
	
	public synchronized void renderRedShift(int x, int y, Sprite sprite)
	{
		// Shifts the colors of the image to be more red
		
		int xp = x - xOffset;
		int yp = y - yOffset;
		
		int[] px = sprite.getPixels();
		
		for(int yl = 0; yl < sprite.SIZE; yl++)
		{
			int ya = yl + yp;
			for(int xl = 0; xl < sprite.SIZE; xl++)
			{
				int xa = xl + xp;
				
				if(!(xa < -sprite.SIZE || xa >= width || ya < 0 || ya >= height))
				{
					if(xa < 0) 
						xa = 0;
					
					int col = px[xl+yl*sprite.SIZE];
					
					if(col != 0xFFFF00DE)
					{
						Color color = new Color(col);
						int g = color.getGreen();
						int b = color.getBlue();
						
						col = new Color(255, g, b).getRGB();
						
						pixels[xa+ya*width] = col;
					}
				}
			}
		}
	}
	
	public synchronized void renderOrangeShiftPercent(int x, int y, Sprite sprite, double amount, double total)
	{
		// Shifts the colors of the image to be more orange
		
		int xp = x - xOffset;
		int yp = y - yOffset;
		
		int[] px = sprite.getPixels();
		
		double dPercentColored = (amount/total);
		
		for(int yl = 0; yl < sprite.SIZE; yl++)
		{
			int ya = yl + yp;
			for(int xl = 0; xl < sprite.SIZE; xl++)
			{
				int xa = xl + xp;
				
				if(!(xa < -sprite.SIZE || xa >= width || ya < 0 || ya >= height))
				{
					if(xa < 0) 
						xa = 0;
					
					int col = px[xl+yl*sprite.SIZE];
					
					if(col != 0xFFFF00DE)
					{
						Color color = new Color(col);
						int r = color.getRed();
						int g = color.getGreen();
						int b = color.getBlue();
						
						int rDifference = 255 - r; // amount away from max red
						int gDifference = 255 - g; // amount away from max green
						
						r = (int) (r + (rDifference * dPercentColored));
						g = (int) (g + (gDifference * (dPercentColored/2.5)));
						
						col = new Color(r, g, b).getRGB();
						
						pixels[xa+ya*width] = col;
					}
				}
			}
		}
	}
	
	public synchronized void renderOrangeShiftPercentSwapColor(int x, int y, Sprite sprite, double amount, double total, int colorToSwap, int newColor)
	{
		// Shifts the colors of the image to be more orange
		
		int xp = x - xOffset;
		int yp = y - yOffset;
		
		int[] px = sprite.getPixels();
		
		double dPercentColored = (amount/total);
		
		for(int yl = 0; yl < sprite.SIZE; yl++)
		{
			int ya = yl + yp;
			for(int xl = 0; xl < sprite.SIZE; xl++)
			{
				int xa = xl + xp;
				
				if(!(xa < -sprite.SIZE || xa >= width || ya < 0 || ya >= height))
				{
					if(xa < 0) 
						xa = 0;
					
					int col = px[xl+yl*sprite.SIZE];
					
					if(col != 0xFFFF00DE)
					{
						if(col == colorToSwap)
							col = newColor;
							
						Color color = new Color(col);
						int r = color.getRed();
						int g = color.getGreen();
						int b = color.getBlue();
						
						int rDifference = 255 - r; // amount away from max red
						int gDifference = 255 - g; // amount away from max green
						
						r = (int) (r + (rDifference * dPercentColored));
						g = (int) (g + (gDifference * (dPercentColored/2.5)));
						
						col = new Color(r, g, b).getRGB();
						
						pixels[xa+ya*width] = col;
					}
				}
			}
		}
	}
	
	public synchronized void clear()
	{
		for(int i = 0; i < pixels.length; i++)
		{
			pixels[i] = backgroundColor;
		}
	}
	
	public int getBackgroundColor()
	{
		return backgroundColor;
	}
	
	public synchronized int[] getPixels()
	{
		return pixels;
	}
	
	public int getWidth()
	{
		return width;
	}
	
	public int getHeight()
	{
		return height;
	}
	
	public void setXOffset(int x)
	{
		xOffset = x;
	}
	
	public void setYOffset(int y)
	{
		yOffset = y;
	}
	
	public int getXOffset()
	{
		return xOffset;
	}
	
	public int getYOffset()
	{
		return yOffset;
	}
	
	public synchronized void setOffset(int xOffset, int yOffset)
	{
		this.xOffset = xOffset;
		this.yOffset = yOffset;
	}
}
