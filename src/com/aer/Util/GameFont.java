package com.aer.Util;

import com.aer.Main.Game;
import com.aer.Textures.Sprite;
import com.aer.Textures.SpriteSheet;

public class GameFont 
{
	private int[] text_a = { 0, 0 };
	private int[] text_b = { 1, 0 };
	private int[] text_c = { 2, 0 };
	private int[] text_d = { 3, 0 };
	private int[] text_e = { 4, 0 };
	private int[] text_f = { 5, 0 };
	private int[] text_g = { 6, 0 };
	private int[] text_h = { 7, 0 };
	private int[] text_i = { 8, 0 };
	private int[] text_j = { 9, 0 };
	private int[] text_k = { 0, 1 };
	private int[] text_l = { 1, 1 };
	private int[] text_m = { 2, 1 };
	private int[] text_n = { 3, 1 };
	private int[] text_o = { 4, 1 };
	private int[] text_p = { 5, 1 };
	private int[] text_q = { 6, 1 };
	private int[] text_r = { 7, 1 };
	private int[] text_s = { 8, 1 };
	private int[] text_t = { 9, 1 };
	private int[] text_u = { 0, 2 };
	private int[] text_v = { 1, 2 };
	private int[] text_w = { 2, 2 };
	private int[] text_x = { 3, 2 };
	private int[] text_y = { 4, 2 };
	private int[] text_z = { 5, 2 };
	private int[] text_0 = { 6, 2 };
	private int[] text_1 = { 7, 2 };
	private int[] text_2 = { 8, 2 };
	private int[] text_3 = { 9, 2 };
	private int[] text_4 = { 0, 3 };
	private int[] text_5 = { 1, 3 };
	private int[] text_6 = { 2, 3 };
	private int[] text_7 = { 3, 3 };
	private int[] text_8 = { 4, 3 };
	private int[] text_9 = { 5, 3 };
	private int[] text_dot = { 6, 3 };
	private int[] text_comma = { 7, 3 };
	private int[] text_exclamation = { 8, 3 };
	private int[] text_question = { 9, 3 };
	private int[] text_apostrophe = { 0, 4 };
	private int[] text_minus = { 1, 4 };
	private int[] text_plus = { 2, 4 };
	private int[] text_percent = { 3, 4 };
	private int[] text_div = { 4, 4 };
	private int[] text_left_parenthese = { 5, 4 };
	private int[] text_right_parenthese = { 6, 4 };
	private int[] text_colon = { 7, 4 };
	private int[] text_semicolon = { 8, 4 };
	private int[] text_space = { 9, 4 };
	
	private int time;
	private int survivalTime;
	private boolean removed = false;
	
	private String text;
	private int x, y;
	private int xMotion, yMotion;
	private boolean fixed;
	
	private int color;
	
	private int[] pixels;
	private int pixels_width;
	
	public GameFont(String text, int x, int y, int color, int xMotion, int yMotion, int survivalTime, boolean fixed)
	{
		this.text = text;
		this.color = color;
		this.fixed = fixed;
		this.x = x;
		this.y = y;
		this.survivalTime = survivalTime;
		
		pixels_width = SpriteSheet.font.getSpriteSize() * text.length();
		pixels = new int[pixels_width * SpriteSheet.font.getSpriteSize()];
		load();
	}
	
	private void load()
	{
		int[] px = SpriteSheet.font.getPixels();
		
		for(int i = 0; i < text.length(); i++)
		{
			for(int yl = 0; yl < SpriteSheet.font.getSpriteSize(); yl++)
			{
				for(int xl = 0; xl < SpriteSheet.font.getSpriteSize(); xl++)
				{	
					int col = 0;
					
					if(text.charAt(i) == 'a' || text.charAt(i) == 'A')
						col = px[(xl+text_a[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_a[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == 'b' || text.charAt(i) == 'B')
						col = px[(xl+text_b[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_b[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == 'c' || text.charAt(i) == 'C')
						col = px[(xl+text_c[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_c[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == 'd' || text.charAt(i) == 'D')
						col = px[(xl+text_d[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_d[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == 'e' || text.charAt(i) == 'E')
						col = px[(xl+text_e[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_e[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == 'f' || text.charAt(i) == 'F')
						col = px[(xl+text_f[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_f[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == 'g' || text.charAt(i) == 'G')
						col = px[(xl+text_g[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_g[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == 'h' || text.charAt(i) == 'H')
						col = px[(xl+text_h[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_h[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == 'i' || text.charAt(i) == 'I')
						col = px[(xl+text_i[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_i[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == 'j' || text.charAt(i) == 'J')
						col = px[(xl+text_j[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_j[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == 'k' || text.charAt(i) == 'K')
						col = px[(xl+text_k[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_k[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == 'l' || text.charAt(i) == 'L')
						col = px[(xl+text_l[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_l[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == 'm' || text.charAt(i) == 'M')
						col = px[(xl+text_m[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_m[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == 'n' || text.charAt(i) == 'N')
						col = px[(xl+text_n[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_n[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == 'o' || text.charAt(i) == 'O')
						col = px[(xl+text_o[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_o[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == 'p' || text.charAt(i) == 'P')
						col = px[(xl+text_p[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_p[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == 'q' || text.charAt(i) == 'Q')
						col = px[(xl+text_q[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_q[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == 'r' || text.charAt(i) == 'R')
						col = px[(xl+text_r[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_r[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == 's' || text.charAt(i) == 'S')
						col = px[(xl+text_s[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_s[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == 't' || text.charAt(i) == 'T')
						col = px[(xl+text_t[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_t[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == 'u' || text.charAt(i) == 'U')
						col = px[(xl+text_u[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_u[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == 'v' || text.charAt(i) == 'V')
						col = px[(xl+text_v[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_v[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == 'w' || text.charAt(i) == 'W')
						col = px[(xl+text_w[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_w[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == 'x' || text.charAt(i) == 'X')
						col = px[(xl+text_x[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_x[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == 'y' || text.charAt(i) == 'Y')
						col = px[(xl+text_y[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_y[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == 'z' || text.charAt(i) == 'Z')
						col = px[(xl+text_z[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_z[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == '0')
						col = px[(xl+text_0[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_0[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == '1')
						col = px[(xl+text_1[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_1[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == '2')
						col = px[(xl+text_2[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_2[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == '3')
						col = px[(xl+text_3[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_3[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == '4')
						col = px[(xl+text_4[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_4[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == '5')
						col = px[(xl+text_5[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_5[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == '6')
						col = px[(xl+text_6[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_6[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == '7')
						col = px[(xl+text_7[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_7[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == '8')
						col = px[(xl+text_8[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_8[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == '9')
						col = px[(xl+text_9[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_9[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == '.')
						col = px[(xl+text_dot[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_dot[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == ',')
						col = px[(xl+text_comma[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_comma[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == '!')
						col = px[(xl+text_exclamation[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_exclamation[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == '?')
						col = px[(xl+text_question[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_question[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == '\'')
						col = px[(xl+text_apostrophe[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_apostrophe[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == '-')
						col = px[(xl+text_minus[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_minus[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == '+')
						col = px[(xl+text_plus[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_plus[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == '%')
						col = px[(xl+text_percent[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_percent[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == '/')
						col = px[(xl+text_div[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_div[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == '(')
						col = px[(xl+text_left_parenthese[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_left_parenthese[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == ')')
						col = px[(xl+text_right_parenthese[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_right_parenthese[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == ':')
						col = px[(xl+text_colon[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_colon[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else if(text.charAt(i) == ';')
						col = px[(xl+text_semicolon[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_semicolon[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					else
						col = px[(xl+text_space[0]*SpriteSheet.font.getSpriteSize()) + (yl+text_space[1]*SpriteSheet.font.getSpriteSize()) *SpriteSheet.font.getWidth()];
					
					
					if(col == 0xFFFFFFFF)
						col = color;
		
					pixels[(xl + i*SpriteSheet.font.getSpriteSize()) + yl*pixels_width] = col;
				}
			}
		}
	}
	
	public void tick()
	{
		time++;
		
		if(time % 5 == 0)
		{
			x += xMotion;
			y += yMotion;
		}
		
		if(survivalTime > 0)
		{
			if(time % survivalTime == 0)
			{
				removed = true;
			}
		}
	}
	
	public void render()
	{
		Game.screen.renderIgnoreOffsets(x, y, new Sprite(pixels, pixels_width, SpriteSheet.font.getSpriteSize()));
	}
	
	public int getX()
	{
		return x;
	}
	
	public int getY()
	{
		return y;
	}
	
	public String getText()
	{
		return text;
	}
	
	public boolean isFixed()
	{
		return fixed;
	}
	
	public int[] getPixels()
	{
		return pixels;
	}
	
	public int getWidth()
	{
		return pixels_width;
	}
	
	public int getHeight()
	{
		return SpriteSheet.font.getSpriteSize();
	}
	
	public boolean isRemoved()
	{
		return removed;
	}
	
	public void setRemoved(boolean removed)
	{
		this.removed = removed;
	}
	
}
