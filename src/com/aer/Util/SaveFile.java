package com.aer.Util;
import java.io.Serializable;

public class SaveFile implements Serializable 
{
	private static final long serialVersionUID = 15L;

	private boolean[] unlockedLevel;
	private int[] levelRating;
	private boolean hasCompletedExtraLevels;
	
	public SaveFile(boolean[] unlockedLevel, int[] levelRating, boolean hasCompletedExtraLevels)
	{
		this.unlockedLevel = unlockedLevel;
		this.levelRating = levelRating;
		this.hasCompletedExtraLevels = hasCompletedExtraLevels;
	}
	
	public boolean[] getUnlockedLevels()
	{
		return unlockedLevel;
	}
	
	public int[] getLevelRating()
	{
		return levelRating;
	}
	
	public boolean getHasCompletedExtraLevels()
	{
		return hasCompletedExtraLevels;
	}
}
