package com.aer.Textures;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class SpriteSheet 
{
	private String path;
	private int spriteSize;
	private final int WIDTH, HEIGHT;
	private int[] pixels;
	
	// Entity
	public static SpriteSheet player = new SpriteSheet("/res/textures/Entity/player.png", 32, 96, 16);
	public static SpriteSheet player_plant = new SpriteSheet("/res/textures/Entity/player_plant.png", 32, 96, 16);
	
	//Tile Sheet
	public static SpriteSheet tiles = new SpriteSheet("/res/textures/tile_sheet.png", 32, 96, 16);
	public static SpriteSheet teleporter_animated =  new SpriteSheet("/res/textures/teleporter_animated.png", 16, 256, 16);
	public static SpriteSheet teleporter_colored_animated =  new SpriteSheet("/res/textures/teleporter_colored_animated.png", 16, 256, 16);
	
	//Levels
	public static SpriteSheet level1 = new SpriteSheet("/res/textures/Levels/Level1.png", 64, 64, 1);
	
	//Gui
	public static SpriteSheet colorBox = new SpriteSheet("/res/textures/Gui/ColorBox.png", 12, 24, 12);
	public static SpriteSheet title = new SpriteSheet("/res/textures/Gui/Title.png", 274, 99, 275);
	public static SpriteSheet start_button = new SpriteSheet("/res/textures/Gui/Start_button.png", 123, 38, 123);
	public static SpriteSheet load_button = new SpriteSheet("/res/textures/Gui/Load_button.png", 123, 38, 123);
	public static SpriteSheet arrow_left = new SpriteSheet("/res/textures/Gui/Arrow_left.png", 20, 19, 20);
	public static SpriteSheet star = new SpriteSheet("/res/textures/Gui/star.png", 6, 6, 6);
	public static SpriteSheet star_empty = new SpriteSheet("/res/textures/Gui/star_empty.png", 6, 6, 6);
	
	// Font
	public static SpriteSheet font = new SpriteSheet("/res/textures/Fonts/font.png", 60, 30, 6);
	
	
	public SpriteSheet(String path, int width, int height, int spriteSize)
	{
		this.path = path;
		WIDTH = width;
		HEIGHT = height;
		this.spriteSize = spriteSize;
		load();
	}
	
	private void load()
	{
		try
		{
			BufferedImage image = ImageIO.read(SpriteSheet.class.getResource(path));
			int w = image.getWidth();
			int h = image.getHeight();
			pixels = new int[w*h];
			image.getRGB(0, 0, w, h, pixels, 0, w);
			System.out.println("Spritesheet loaded successfully.");
		}
		catch(IOException e)
		{
			System.out.println("Oops! Something went wrong!");
			e.printStackTrace();
		}
	}
	
	public int[] getPixels()
	{
		return pixels;
	}
	
	public int getSpriteSize()
	{
		return spriteSize;
	}
	
	public int getWidth()
	{
		return WIDTH;
	}
	
	public int getHeight()
	{
		return HEIGHT;
	}
	
}
