package com.aer.Textures;

public class Sprite 
{
	public final int SIZE, WIDTH, HEIGHT;
	private int[] pixels;
	private SpriteSheet sheet;
	
	private int sheet_xt, sheet_yt;
	
	//------------------Sprites--------------------
	
	public static Sprite editable_tile= new Sprite(0, 0, SpriteSheet.tiles);
	public static Sprite solid_tile = new Sprite(1, 0, SpriteSheet.tiles);
	public static Sprite blank_tile = new Sprite(0, 1, SpriteSheet.tiles);
	public static Sprite blank_tile_emptyWindow = new Sprite(0, 2, SpriteSheet.tiles);
	public static Sprite blank_tile_personEmptyWindow = new Sprite(0, 3, SpriteSheet.tiles);
	public static Sprite blank_tile_personWindow = new Sprite(1, 2, SpriteSheet.tiles);
	public static Sprite blank_tile_plantWindow = new Sprite(1, 1, SpriteSheet.tiles);
	public static Sprite plant = new Sprite(1, 3, SpriteSheet.tiles);
	public static Sprite colorBoxUnselected = new Sprite(0, 0, SpriteSheet.colorBox);
	public static Sprite colorBoxSelected = new Sprite(0, 1, SpriteSheet.colorBox);
	public static Sprite star = new Sprite(0, 0, SpriteSheet.star);
	public static Sprite star_empty = new Sprite(0, 0, SpriteSheet.star_empty);

	//---------------------------------------------
	
	public Sprite(int xt, int yt, SpriteSheet sheet)
	{
		sheet_xt = xt;
		sheet_yt = yt;
		
		this.sheet = sheet;
		
		SIZE = sheet.getSpriteSize();
		WIDTH = SIZE;
		HEIGHT = SIZE;
		
		pixels = new int[SIZE*SIZE];
		load();
	}
	
	public Sprite(int[] pixels, int size)
	{
		SIZE = size;
		WIDTH = SIZE;
		HEIGHT = SIZE;
		
		this.pixels = pixels;
	}
	
	public Sprite(int[] pixels, int width, int height)
	{
		SIZE = height;
		
		WIDTH = width;
		HEIGHT = height;
		
		this.pixels = pixels;
	}
	
	public int[] getPixels()
	{
		return pixels;
	}
	
	private void load()
	{
		int[] px = sheet.getPixels();
		
		for(int y = 0; y < SIZE; y++)
		{
			for(int x = 0; x < SIZE; x++)
			{
				pixels[x+y*SIZE] = px[(x + (sheet_xt*SIZE)) + (y + (sheet_yt*SIZE)) * sheet.getWidth()];
			}
		}
	}
}
