package com.aer.Textures;

public class AnimatedSprite 
{
	private SpriteSheet sheet;
	
	private int[] pixels;
	private int xt, yt;
	private int animLength;
	
	private int animFrame;
	
	private int animSpeed; // AnimSpeed is the speed at which an animation changes. --  animSpeed/60 = number of seconds between updates  (ex. 60 = ~1 second)
	private int time;
	
	public int totalLoops; // Number of times the animatedSprite has looped
	
	public AnimatedSprite(SpriteSheet sheet, int xt, int yt, int animationLength, int animationSpeed)
	{
		this.sheet = sheet;
		this.xt = xt;
		this.yt = yt;
		
		if(animationLength > 0)
			animLength = animationLength;
		else
			System.err.println("Unsuccessful Animation Set: OutOfBounds (animation frame)");
		
		if(animationSpeed > 0)
			animSpeed = animationSpeed;
		else
			System.err.println("Unsuccessful Animation Set: OutOfBounds (animation speed)");
		
		pixels = new int[sheet.getHeight() * sheet.getWidth()];
		load();
	}
	
	private void load()
	{
		//Loads section of the spriteSheet
		int[] px = sheet.getPixels();
		
		for(int y = 0; y < (sheet.getSpriteSize() * animLength); y++)
		{
			for(int x = 0; x < sheet.getSpriteSize(); x++)
			{
				pixels[x+y*sheet.getSpriteSize()] = px[(x + xt*sheet.getSpriteSize()) + (y + yt * sheet.getSpriteSize())*sheet.getWidth()];
			}
		}
	}
	
	public void setAnimFrame(int a)
	{
		if(a < animLength && a >= 0)
		{
			animFrame = a;
		}
		else
		{
			System.err.println("Unsuccessful Animation Set: OutOfBounds (animation frame)");
		}
	}
	
	public void setAnimSpeed(int s)
	{
		if(s > 0)
		{
			animSpeed = s;
		}
		else
		{
			System.err.println("Unsuccessful Animation Set: OutOfBounds (animation speed)");
		}
	}
	
	public Sprite getSprite()
	{
		int[] spritePixels = new int[sheet.getSpriteSize() * sheet.getSpriteSize()];
		
		for(int y = 0; y < sheet.getSpriteSize(); y++)
		{
			for(int x = 0; x < sheet.getSpriteSize(); x++)
			{
				spritePixels[x+y*sheet.getSpriteSize()] = pixels[x + (y + animFrame * sheet.getSpriteSize())*sheet.getSpriteSize()];
			}
		}
		
		return new Sprite(spritePixels, sheet.getSpriteSize());
	}
	
	public Sprite getSprite(int index)
	{
		int[] spritePixels = new int[sheet.getSpriteSize() * sheet.getSpriteSize()];
		
		if(index >= animLength)
			index = 0;
		
		for(int y = 0; y < sheet.getSpriteSize(); y++)
		{
			for(int x = 0; x < sheet.getSpriteSize(); x++)
			{
				spritePixels[x+y*sheet.getSpriteSize()] = pixels[x + (y + index * sheet.getSpriteSize())*sheet.getSpriteSize()];
			}
		}
		
		return new Sprite(spritePixels, sheet.getSpriteSize());
	}

	public void tick()
	{
		time++;
		
		if(time % animSpeed == 0)
		{
			animFrame++;
			time = 0;
			
			if(animFrame >= animLength)
			{
				animFrame = 0;
				totalLoops++;
			}
		}
	}
	
	public int getFrameNumber()
	{
		return animFrame;
	}
	
	public int getLength()
	{
		return animLength;
	}
}
