package com.aer.Entity;

import java.util.Random;

import com.aer.Levels.Level;
import com.aer.Main.Game;
import com.aer.Textures.Sprite;

public class Particle extends Entity
{
	private int color, size, life;
	private double xs, ys;
	private Sprite sprite;
	
	private final double ACCEL_DEFAULT = 0.055;
	
	private Random rand = new Random();
	
	public Particle(double x, double y, Level level, int color, int size, int life, boolean varyLifeSpan)
	{
		super(x, y, level);
		this.color = color;
		
		if(size >= 1)
		{
			this.size = size;
		}
		else
		{
			System.out.println("Invalid Particle Size! Setting Default: 1x1");
			this.size = 1;
		}
		
		if(life >= 1)
		{
			if(varyLifeSpan)
				this.life = rand.nextInt(life)+life;
			else
				this.life = life;
		}
		else
		{
			System.out.println("Invalid Particle Life! Setting Default: 10");
			this.life = 10;
		}
		
		xs = rand.nextGaussian();
		ys = rand.nextGaussian();
		
		load();
	}
	
	private void load()
	{
		int[] pixels = new int[size*size];
		
		for(int y = 0; y < size; y++)
		{
			for(int x = 0; x < size; x++)
			{
				pixels[x+y*size] = color;
			}
		}
		
		sprite = new Sprite(pixels, size);
	}
	
	public void tick() 
	{
		if(collision(xs, 0))
		{
			xs *= -1;
		}
		
		if(collision(0, ys))
		{
			ys *= -1;
		}
		
		ys += ACCEL_DEFAULT;
		
		x += xs;
		y += ys;
		
		life--;
		
		if(life <= 0)
			setRemoved(true);
	}
	
	private boolean collision(double xs, double ys)
	{
		boolean solid = false;
		
		for(int c = 0; c < 4; c++)
		{
			int xt = (int) ((x+xs) + c % 2 * size) >> 4;
			int yt = (int) ((y+ys) + c / 2 * size) >> 4;
			
			if(level.getTileData(xt, yt).SOLID)
				solid = true;
		}
		
		return solid;
	}

	public void render() 
	{
		Game.screen.render((int)x, (int)y, sprite);
	}

}
