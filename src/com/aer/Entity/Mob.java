package com.aer.Entity;

import com.aer.Levels.Level;
import com.aer.Tiles.TileData;

public abstract class Mob extends Entity
{
	protected final double RAW_SPEED = 1.0;
	protected double speed = 1.0;
	
	public final int SIZE = 16;
	
	
	public Mob(double x, double y, Level level) 
	{
		super(x, y, level);
	}
	
	public abstract void tick();
	public abstract void render();
	
	protected void move(double xs, double ys)
	{
		if(xs != 0 && ys != 0)
		{
			move(xs, 0);
			move(0, ys);
		}
		else
		{
			if(!tileSolid((int)xs, 0))
				x += xs;
			
			if(!tileSolid(0, (int)ys))
				y += ys;
		}
	}
	
	protected boolean tileSolid(double xs, double ys)
	{
		boolean solid = false;
		
		if(xs > 0)
			xs = Math.ceil(xs);
		else if (xs < 0)
			xs = Math.floor(xs);
		
		if(ys > 0)
			ys = Math.ceil(ys);
		else if(ys < 0)
			ys = Math.floor(ys);
		
		
		for(int c = 0; c < 4; c++)
		{
			int xt = (int)(((x + xs) - (c % 2) * 7 + 11)) >> 4;
			int yt = (int)(((y + ys) - (c / 2) * 15 + 15)) >> 4;
			
			if(level.getTileData(xt, yt).SOLID)
				solid = true;
		}
		
		return solid;
	}
	
	protected boolean mobCushionTileContact(double xs, double ys)
	{
		// Checks whether the mob is touching a cushion tile
		
		boolean cushion = false;
		
		if(xs > 0)
			xs = Math.ceil(xs);
		else if (xs < 0)
			xs = Math.floor(xs);
		
		if(ys > 0)
			ys = Math.ceil(ys);
		else if(ys < 0)
			ys = Math.floor(ys);
		
		
		for(int c = 0; c < 4; c++)
		{
			int xt = (int)(((x + xs) - (c % 2) * 7 + 11)) >> 4;
			int yt = (int)(((y + ys) - (c / 2) * 15 + 15)) >> 4;
			
			if(level.getTileData(xt, yt).COLOR == Player.PLAYER_COL_PURPLE)
				cushion = true;
		}
		
		return cushion;
	}
	
	protected TileData getTileDataAtShift(int xs, int ys)
	{
		// Returns tile data at x shift and y shift
		TileData td = null;
		
		int xt = (int)(x+xs) >> 4;
		int yt = (int)(y+ys) >> 4;
			
		td = level.getTileData(xt, yt);
		
		return td;
	}
	
	protected TileData getTileDataAtMob()
	{
		//Returns tile data at the mob's feet
		TileData td = null;
		
		int xt = (int)(x+(Player.SIZE/2)) >> 4;
		int yt = (int)(y + Player.SIZE+1) >> 4;
			
		td = level.getTileData(xt, yt);
		
		return td;
	}
	
	protected TileData getTileDataAtMobCenter()
	{
		// Returns tile data at the center of the mob
		TileData td = null;
		
		int xt = (int)(x+(Player.SIZE/2)) >> 4;
		int yt = (int)(y + (Player.SIZE/2)) >> 4;
			
		td = level.getTileData(xt, yt);
		
		return td;
	}
	
	protected int getTileNumberAtMob()
	{
		// Returns tiles index in tile array (in current level) at mob's feet
		int xt = (int)(x+(Player.SIZE/2)) >> 4;
		int yt = (int)(y + Player.SIZE+1) >> 4;
		
		return level.getTileNumber(xt, yt);
	}
}
