package com.aer.Entity;

import com.aer.Levels.Level;
import com.aer.Main.Game;
import com.aer.Main.Keyboard;
import com.aer.Main.Mouse;
import com.aer.Textures.AnimatedSprite;
import com.aer.Textures.Sprite;
import com.aer.Textures.SpriteSheet;
import com.aer.Tiles.TileData;

public class Player extends Mob
{
	private Keyboard key;
	
	private final int RAW_WALK_ANIMATION_SPEED = 2;
	private AnimatedSprite west = new AnimatedSprite(SpriteSheet.player, 0, 0, 6, RAW_WALK_ANIMATION_SPEED);
	private AnimatedSprite east = new AnimatedSprite(SpriteSheet.player, 1, 0, 6, RAW_WALK_ANIMATION_SPEED);
	private AnimatedSprite current;
	
	public static final int SIZE = 16;
	
	protected final double RAW_SPEED = 2.0;
	protected double speed = 2.0;
	
	private double xs, ys;
	
	private boolean requestReset = false;
	private boolean stopPlayerRender = false;
	private boolean stopPlayerMotion = false;
	
	//Player teleportation
	private boolean requestNextLevel = false;
	private final int DEFAULT_TELEPORT_TIMER = 60;
	private int teleportTimer = DEFAULT_TELEPORT_TIMER;
	private boolean beingTeleported = false;
	
	// Player Death
	private double previousFallSpeed = 0.0;
	private boolean hasBeenKilled = false;
	private int deathTimer = 60;
	
	// Fall physics
	private boolean falling = false;
	private boolean jumping = false;
	private double fallSpeed = 0;
	private final double ACCEL_DEFAULT = 0.07;
	private double accel = ACCEL_DEFAULT;
	private final double YS_SETTER = 2.5;
	private final double YS_JUMP = -5;
	private double ys_jump = YS_JUMP;
	private boolean stop_accel = false;
	
	// Color data
	public static final int PLAYER_COL_BLUE = 0xFF0094FF;
	public static final int PLAYER_COL_ORANGE = 0xFFFF802B;
	public static final int PLAYER_COL_PURPLE = 0xFFAA26FF;
	public static final int PLAYER_COL_LBLUE = 0xFFB2DFFF;
	public static final int PLAYER_COL_GREEN = 0xFF00D819;
	
	
	// Universal Player Data
	public static int selectedColor = 0xFFFFFFFF;
	public static boolean unlockedBlue = false; // jump
	public static boolean unlockedOrange = false; // speed
	public static boolean unlockedPurple = false; // cushion
	public static boolean unlockedLBlue = false; // ghost
	public static boolean unlockedGreen = false; // antigravity
	
	public static int colorIndex = 0;
	public static int totalUnlockedColors = 0;
	
	public static boolean hasCompletedExtraLevels = false;
	
	// LEVELS:                                 1     2     3       4      5      6     7       8     9      10     11     12     13     14     15     16     17      18     19    20     21     22     23     24     25     26     27     28     29     30
	public static boolean[] unlockedLevel = { true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false };
	//public static boolean[] unlockedLevel = { true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true };
	
	public static int[] levelRating = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	//public static int[] levelRating = { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 };
	
	// Speed effect
	private int speedTimer = 0;
	private double speedBoost = 0.0;
	private int lastMovementDir = 0;
	
	// Teleport
	private int teleportColor = 0;
	private boolean readyToTeleport = false;
	
	// Player crushed
	private int crushed_counter = 0; // used to time how long a player is stuck for (if beyond a certain time, the player is killed)
	
	public Player(int x, int y, Level level, Keyboard key) 
	{
		super(x, y, level);
	
		if(hasCompletedExtraLevels)
		{
			west = new AnimatedSprite(SpriteSheet.player_plant, 0, 0, 6, RAW_WALK_ANIMATION_SPEED);
			east = new AnimatedSprite(SpriteSheet.player_plant, 1, 0, 6, RAW_WALK_ANIMATION_SPEED);
		}
		
		current = east;
		this.key = key;
	}

	public void tick() 
	{
		//System.out.println("x: " + x + ", y: " + y);
		
		timerTick();
		walk();
		tickColorPalette();
		tickTeleport();
		
		if(key.key_r)
			requestReset = true;
		
		if(hasBeenKilled && deathTimer <= 0)
			requestReset = true;
	}
	
	private void walk()
	{
		// Handles Player movement
		xs = 0.0;
		ys = YS_SETTER;
		speed = RAW_SPEED;
		
		if(key.right)
		{
			xs += speed; 
			current = east;
		}
		else if(key.left)
		{
			xs -= speed;
			current = west;
		}
		
		current.tick();
		
		if(xs == 0)
			current.setAnimFrame(0);
		
		
		determineTileEffectData();
		determineYMovement();
		
		// Factor in speedboost for x movement
		if(speedTimer > 0)
			xs = xs + (xs * speedBoost);
		else
			speedBoost = 0;

		previousFallSpeed = ys;
		
		move(xs, ys);
		handleGroundCollision();
		handlePlayerCrushed();
	}

	private void determineTileEffectData() // Handles special tile collision and y movement
	{
		TileData td = getTileDataAtMob();
		
		// Tile effects
		if(td.COLOR == PLAYER_COL_BLUE) // Jump
		{
			int tileX = getTileNumberAtMob();
			int numConnected = 1;
			boolean endRight = false, endLeft = false;
			int offset = 1;
			
			while(!endRight) // check tiles to the right of player for blue tiles
			{
				if(level.getTileData(tileX+offset).COLOR == PLAYER_COL_BLUE)
				{
					offset++;
					numConnected++;
				}
				else
				{
					endRight = true;
				}
			}
			
			offset = -1;
			
			while(!endLeft) // check tiles to the left of player for blue tiles
			{
				if(level.getTileData(tileX+offset).COLOR == PLAYER_COL_BLUE)
				{
					offset--;
					numConnected++;
				}
				else
				{
					endLeft = true;
				}
			}
			
			ys_jump = YS_JUMP*(1 + (numConnected*0.2)); 
		}
		else if(td.COLOR == PLAYER_COL_ORANGE) // Speed
		{
			speedTimer = 180;
			
			if(lastMovementDir == 0)
			{
				if(xs > 0)
					lastMovementDir = 1;
				else if(xs < 0)
					lastMovementDir = -1;
			}
			else if(lastMovementDir > 0)
			{
				if(xs > 0)
				{
					speedBoost += 0.03;
				}
				else if(xs < 0)
				{
					speedBoost = 0;
					lastMovementDir = -1;
				}
				else
				{
					speedBoost = 0;
					lastMovementDir = 0;
				}
			}
			else if(lastMovementDir < 0)
			{
				if(xs < 0)
				{
					speedBoost += 0.03;
				}
				else if(xs > 0)
				{
					speedBoost = 0;
					lastMovementDir = 1;
				}
				else
				{
					speedBoost = 0;
					lastMovementDir = 0;
				}
			}
		}
		else if(td.COLOR == 0xFF00D819) // Acts as antigravity and slight jump booster
		{
			int tileX = getTileNumberAtMob();
			int numConnected = 1;
			boolean endRight = false, endLeft = false;
			int offset = 1;
			
			while(!endRight)
			{
				if(level.getTileData(tileX+offset).COLOR == PLAYER_COL_GREEN)
				{
					offset++;
					numConnected++;
				}
				else
				{
					endRight = true;
				}
			}
			
			offset = -1;
			
			while(!endLeft)
			{
				if(level.getTileData(tileX+offset).COLOR == PLAYER_COL_GREEN)
				{
					offset--;
					numConnected++;
				}
				else
				{
					endLeft = true;
				}
			}
			
			
			if(numConnected >= 6)
				accel = 0.01;
			else if(numConnected == 5)
				accel = 0.02;
			else if(numConnected == 4)
				accel = 0.03;
			else if(numConnected == 3)
				accel = 0.04;
			else if(numConnected == 2)
				accel = 0.05;
			else if(numConnected == 1)
				accel = 0.06;
			else
				accel = ACCEL_DEFAULT;
			
			ys_jump = YS_JUMP*(1 + (numConnected*0.05)); 
		}
	}
	
	private void determineYMovement()
	{
		// Fall physics
		if(key.space)
		{
			jumping = true;
		}
				
		if(jumping)
		{
			ys += ys_jump;
		}
		
		if(falling && !beingTeleported && !stop_accel)
		{
			fallSpeed += accel;
		}
		
		ys += fallSpeed;
	}
	
	private void checkFallDeath()
	{
		// Checks for fall death by player
		if(previousFallSpeed > 6.5)
		{
			if(getTileDataAtMob().COLOR != PLAYER_COL_PURPLE && getTileDataAtMob().COLOR != PLAYER_COL_LBLUE)
			{
				if(getTileDataAtMob().SOLID == false && mobCushionTileContact(xs, ys))
				{	
					// Do nothing
				}
				else
				{
					// Killed by fall damage
					level.addEntity(new ParticleSpawner(x+SIZE/2, y+SIZE/2, level, 0xFFAF0000, 2, 30, 75, 1, true, true));
					hasBeenKilled = true;
					stopPlayerMotion = true;
					stopPlayerRender = true;
				}
			}
		}
	}
	
	private void handlePlayerCrushed()
	{
		if(getTileDataAtMobCenter().SOLID && !(Math.abs(ys) > 5) && !(Math.abs(xs) > 4.5))
		{
			crushed_counter++;
			
			if(crushed_counter > 20)
			{
				level.addEntity(new ParticleSpawner(x+SIZE/2, y+SIZE/2, level, 0xFFAF0000, 2, 30, 75, 1, true, true));
				hasBeenKilled = true;
				stopPlayerMotion = true;
				stopPlayerRender = true;
			}
		}
		else
		{
			crushed_counter = 0;
		}
	}
	
	private void handleGroundCollision()
	{
		if(!((getTileDataAtMob().COLOR == PLAYER_COL_LBLUE || getTileDataAtMobCenter().COLOR == PLAYER_COL_LBLUE || getTileDataAtShift(SIZE/2, -1).COLOR == PLAYER_COL_LBLUE) && ys > 5) && !((getTileDataAtMob().COLOR == PLAYER_COL_LBLUE || getTileDataAtMobCenter().COLOR == PLAYER_COL_LBLUE || getTileDataAtShift(SIZE/2, -1).COLOR == PLAYER_COL_LBLUE) && ys < -5)) // check y coordinates for ghost tiles (if there are, the "else" is run)
		{
			stop_accel = false;
			
			if(tileSolid(0, 1))
			{
				// Touching solid ground
				
				falling = false;
				jumping = false;
				fallSpeed = 0;
				accel = ACCEL_DEFAULT;
				ys_jump = YS_JUMP;
				
				checkFallDeath();
			}
			else if(tileSolid(0, -1))
			{
				// Touching ceiling
				fallSpeed = Math.abs(ys_jump + YS_SETTER);
				
				if(accel < ACCEL_DEFAULT)
					accel = ACCEL_DEFAULT;
			}
			else
			{
				falling = true;
			}
		}
		else
		{
			// Ghost tiles present
			stop_accel = true;
		}
	}
	
	protected void move(double xs, double ys)
	{
		
		while(xs != 0 && !stopPlayerMotion)
		{	
			if(Math.abs(xs) >= 1)
			{		
				if(!tileSolid(abs(xs), 0))
				{
					x += abs(xs);
				}
				else if((getTileDataAtShift(17, 0).COLOR == PLAYER_COL_LBLUE || getTileDataAtMobCenter().COLOR == PLAYER_COL_LBLUE || getTileDataAtShift(-1, 0).COLOR == PLAYER_COL_LBLUE) && Math.abs(xs) > 4.5)
				{
					x += abs(xs);
				}
				
				xs -= abs(xs);
			}
			else // handles movement that is not a whole-number
			{
				if(!tileSolid(abs(xs), 0))
				{
					x += xs;
				}
				else if((getTileDataAtShift(17, 0).COLOR == PLAYER_COL_LBLUE || getTileDataAtMobCenter().COLOR == PLAYER_COL_LBLUE || getTileDataAtShift(-1, 0).COLOR == PLAYER_COL_LBLUE) && Math.abs(xs) > 4.5)
				{
					x += xs;
				}
				
				xs = 0;
			}
			
			if((getTileDataAtShift(SIZE/2, SIZE+1).COLOR == 0xFFFF0000 || getTileDataAtShift(SIZE/2, -1).COLOR == 0xFFFF0000 || getTileDataAtShift(-1, SIZE/2).COLOR == 0xFFFF0000 || getTileDataAtShift(SIZE+1, SIZE/2).COLOR == 0xFFFF0000 || getTileDataAtShift(-1, -1).COLOR == 0xFFFF0000 || getTileDataAtShift(SIZE+1, -1).COLOR == 0xFFFF0000 || getTileDataAtShift(-1, -1).COLOR == 0xFFFF0000 || getTileDataAtShift(-1, SIZE+1).COLOR == 0xFFFF0000 || getTileDataAtShift(SIZE+1, SIZE+1).COLOR == 0xFFFF0000) && !hasBeenKilled) // Fire tile
			{
				level.addEntity(new ParticleSpawner(x+SIZE/2, y+SIZE/2, level, 0xFFAF0000, 2, 30, 75, 1, true, true));
				hasBeenKilled = true;
				stopPlayerMotion = true;
				stopPlayerRender = true;
			}
		}
		
		
		while(ys != 0 && !stopPlayerMotion)
		{
			if(Math.abs(ys) >= 1)
			{
				if(!tileSolid(0, abs(ys)))
				{
					y += abs(ys);
				}
				else if((getTileDataAtMob().COLOR == PLAYER_COL_LBLUE || getTileDataAtMobCenter().COLOR == PLAYER_COL_LBLUE || getTileDataAtShift(SIZE/2, 1).COLOR == PLAYER_COL_LBLUE) && ys > 5)
				{
					y += abs(ys);
				}
				else if((getTileDataAtMob().COLOR == PLAYER_COL_LBLUE || getTileDataAtMobCenter().COLOR == PLAYER_COL_LBLUE || getTileDataAtShift(SIZE/2, -1).COLOR == PLAYER_COL_LBLUE) && ys < -5)
				{
					y += abs(ys);
				}
				ys -= abs(ys);
			}
			else
			{
				if(!tileSolid(0, abs(ys)))
				{
					y += ys;
				}
				else if((getTileDataAtMob().COLOR == PLAYER_COL_LBLUE || getTileDataAtMobCenter().COLOR == PLAYER_COL_LBLUE || getTileDataAtShift(SIZE/2, 1).COLOR == PLAYER_COL_LBLUE) && ys > 5)
				{
					y += ys;
				}
				else if((getTileDataAtMob().COLOR == PLAYER_COL_LBLUE || getTileDataAtMobCenter().COLOR == PLAYER_COL_LBLUE || getTileDataAtShift(SIZE/2, -1).COLOR == PLAYER_COL_LBLUE) && ys < -5)
				{
					y += abs(ys);
				}
				ys = 0;
			}
			
			if((getTileDataAtShift(SIZE/2, SIZE+1).COLOR == 0xFFFF0000 || getTileDataAtShift(SIZE/2, -1).COLOR == 0xFFFF0000 || getTileDataAtShift(-1, SIZE/2).COLOR == 0xFFFF0000 || getTileDataAtShift(SIZE+1, SIZE/2).COLOR == 0xFFFF0000 || getTileDataAtShift(-1, -1).COLOR == 0xFFFF0000 || getTileDataAtShift(SIZE+1, -1).COLOR == 0xFFFF0000 || getTileDataAtShift(-1, -1).COLOR == 0xFFFF0000 || getTileDataAtShift(-1, SIZE+1).COLOR == 0xFFFF0000 || getTileDataAtShift(SIZE+1, SIZE+1).COLOR == 0xFFFF0000) && !hasBeenKilled) // Fire tile
			{
				level.addEntity(new ParticleSpawner(x+SIZE/2, y+SIZE/2, level, 0xFFAF0000, 2, 30, 75, 1, true, true));
				hasBeenKilled = true;
				stopPlayerMotion = true;
				stopPlayerRender = true;
			}
		}	
	}
	
	private void tickColorPalette()
	{
		if(totalUnlockedColors > 0)
		{
			colorIndex += Mouse.getMouseWheelMotion(); // mouseWheelMotion = -1 is UP, mouseWheelMotion = 1 is DOWN
			
			if(colorIndex <= 0)
				colorIndex = totalUnlockedColors;
			else if(colorIndex > totalUnlockedColors)
				colorIndex = 1;
			
			
			if((key.key1 && unlockedBlue) || (colorIndex == 1 && unlockedBlue))
			{
				colorIndex = 1;
				selectedColor = PLAYER_COL_BLUE;
			}
			
			if((key.key2 && unlockedOrange) || (colorIndex == 2 && unlockedOrange))
			{
				colorIndex = 2;
				selectedColor = PLAYER_COL_ORANGE;
			}
			
			if((key.key3 && unlockedPurple) || (colorIndex == 3 && unlockedPurple))
			{
				colorIndex = 3;
				selectedColor = PLAYER_COL_PURPLE;
			}
			
			if((key.key4 && unlockedLBlue) || (colorIndex == 4 && unlockedLBlue))
			{
				colorIndex = 4;
				selectedColor = PLAYER_COL_LBLUE;
			}
			
			if((key.key5 && unlockedGreen) || (colorIndex == 5 && unlockedGreen))
			{
				colorIndex = 5;
				selectedColor = PLAYER_COL_GREEN;
			}
		}
	}
	
	private void renderColorPalette()
	{
		if(unlockedBlue)
		{
			if(selectedColor == PLAYER_COL_BLUE)
				Game.screen.renderIgnoreOffsetsSwapColor(50, 10, Sprite.colorBoxSelected, 0xFFBC00A6, PLAYER_COL_BLUE);
			else
				Game.screen.renderIgnoreOffsetsSwapColor(50, 10, Sprite.colorBoxUnselected, 0xFFBC00A6, PLAYER_COL_BLUE);
		}
		
		if(unlockedOrange)
		{
			if(selectedColor == PLAYER_COL_ORANGE)
				Game.screen.renderIgnoreOffsetsSwapColor(70, 10, Sprite.colorBoxSelected, 0xFFBC00A6, PLAYER_COL_ORANGE);
			else
				Game.screen.renderIgnoreOffsetsSwapColor(70, 10, Sprite.colorBoxUnselected, 0xFFBC00A6, PLAYER_COL_ORANGE);
		}
		
		if(unlockedPurple)
		{
			if(selectedColor == PLAYER_COL_PURPLE)
				Game.screen.renderIgnoreOffsetsSwapColor(90, 10, Sprite.colorBoxSelected, 0xFFBC00A6, PLAYER_COL_PURPLE);
			else
				Game.screen.renderIgnoreOffsetsSwapColor(90, 10, Sprite.colorBoxUnselected, 0xFFBC00A6, PLAYER_COL_PURPLE);
		}
		
		if(unlockedLBlue)
		{
			if(selectedColor == PLAYER_COL_LBLUE)
				Game.screen.renderIgnoreOffsetsSwapColor(110, 10, Sprite.colorBoxSelected, 0xFFBC00A6, PLAYER_COL_LBLUE);
			else
				Game.screen.renderIgnoreOffsetsSwapColor(110, 10, Sprite.colorBoxUnselected, 0xFFBC00A6, PLAYER_COL_LBLUE);
		}
		
		if(unlockedGreen)
		{
			if(selectedColor == PLAYER_COL_GREEN)
				Game.screen.renderIgnoreOffsetsSwapColor(130, 10, Sprite.colorBoxSelected, 0xFFBC00A6, PLAYER_COL_GREEN);
			else
				Game.screen.renderIgnoreOffsetsSwapColor(130, 10, Sprite.colorBoxUnselected, 0xFFBC00A6, PLAYER_COL_GREEN);
		}
	}
	
	private void timerTick()
	{
		if(speedTimer > 0)
			speedTimer--;
		
		if(deathTimer > 0 && hasBeenKilled)
			deathTimer--;
		
		if(teleportTimer > 0 && beingTeleported)
		{
			teleportTimer--;
		}
	}
	
	private int abs(double value)
	{
		if (value < 0) return -1;
		else return 1;
	}
	
	public void render() 
	{
		Sprite currentSprite = current.getSprite();
		
		if(!stopPlayerRender)
		{
			if(selectedColor == PLAYER_COL_BLUE)
				Game.screen.renderOrangeShiftPercentSwapColor((int)x, (int)y, currentSprite, speedTimer, 180, 0xFFFF006E, PLAYER_COL_BLUE);
			else if(selectedColor == PLAYER_COL_ORANGE)
				Game.screen.renderOrangeShiftPercentSwapColor((int)x, (int)y, currentSprite, speedTimer, 180, 0xFFFF006E, PLAYER_COL_ORANGE);
			else if(selectedColor == PLAYER_COL_PURPLE)
				Game.screen.renderOrangeShiftPercentSwapColor((int)x, (int)y, currentSprite, speedTimer, 180, 0xFFFF006E, PLAYER_COL_PURPLE);
			else if(selectedColor == PLAYER_COL_LBLUE)
				Game.screen.renderOrangeShiftPercentSwapColor((int)x, (int)y, currentSprite, speedTimer, 180, 0xFFFF006E, PLAYER_COL_LBLUE);
			else if(selectedColor == PLAYER_COL_GREEN)
				Game.screen.renderOrangeShiftPercentSwapColor((int)x, (int)y, currentSprite, speedTimer, 180, 0xFFFF006E, PLAYER_COL_GREEN);
			else
				Game.screen.renderOrangeShiftPercentSwapColor((int)x, (int)y, currentSprite, speedTimer, 180, 0xFFFF006E, 0xFF6B6B6B);
		}
		
		renderColorPalette();
	}
	
	public boolean isReadyToTeleport()
	{
		return readyToTeleport;
	}
	
	public int getTeleportColor()
	{
		return teleportColor;
	}
	
	public void resetTeleport()
	{
		readyToTeleport = false;
		stopPlayerRender = false;
		teleportColor = 0;
		stopPlayerMotion = false;
		beingTeleported = false;
		teleportTimer = DEFAULT_TELEPORT_TIMER;
	}
	
	private void tickTeleport()
	{
		// Teleport check (next level)
		if(getTileDataAtMobCenter().TELEPORTER && !beingTeleported)
		{
			teleportColor = getTileDataAtMobCenter().COLOR;
			stopPlayerMotion = true;
			beingTeleported = true;
		}
		
		if(beingTeleported)
		{
			if((teleportTimer % 10 >= 0 && teleportTimer % 10 <= 5) || teleportTimer <= 10)
			{
				stopPlayerRender = true;
			}
			else
			{
				stopPlayerRender = false;
			}
			
			if(teleportTimer == 15)
			{
				if(teleportColor != 0xFFFF7F7F)
					level.addEntity(new ParticleSpawner(x+SIZE/2, y+SIZE/2, level, teleportColor, 2, 30, 30, 2, true, true));
				else
					level.addEntity(new ParticleSpawner(x+SIZE/2, y+SIZE/2, level, 0xFF00EFEF, 2, 30, 30, 2, true, true));
			}
			
			if(teleportTimer <= 0)
			{
				readyToTeleport = true;
			}
			
			if(readyToTeleport)
			{
				if(teleportColor == 0xFFFF7F7F)
					requestNextLevel = true;
			}
		}
	}
	
	public boolean hasRequestReset()
	{
		return requestReset;
	}
	
	public boolean hasRequestedNextLevel()
	{
		return requestNextLevel;
	}
}
