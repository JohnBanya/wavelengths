package com.aer.Entity;

import com.aer.Levels.Level;

public class ParticleSpawner extends Entity
{
	private int color, size, life, numberToSpawn, speedPerSpawn;
	private int time;
	private boolean varyParticleLifeSpan;
	
	
	public ParticleSpawner(double x, double y, Level level, int color, int size, int particleLife, int numberToSpawn, int speedPerSpawn, boolean varyParticleLifeSpan, boolean burst) 
	{
		super(x, y, level);
		
		this.color = color;
		this.varyParticleLifeSpan = varyParticleLifeSpan;
		
		if(size >= 1)
		{
			this.size = size;
		}
		else
		{
			System.out.println("Invalid Particle (spawner) Size! Setting Default: 1x1");
			this.size = 1;
		}
		
		if(particleLife >= 1)
		{
			life = particleLife;
		}
		else
		{
			System.out.println("Invalid Particle (spawner) Life! Setting Default: 1");
			life = 1;
		}
		
		if(numberToSpawn >= 1)
		{
			this.numberToSpawn = numberToSpawn;
		}
		else
		{
			System.out.println("Invalid Particle (spawner) numberToSpawn! Setting Default: 10");
			this.numberToSpawn = 10;
		}
		
		if(speedPerSpawn >= 1)
		{
			this.speedPerSpawn = speedPerSpawn;
		}
		else
		{
			System.out.println("Invalid Particle (spawner) speedPerSpawn! Setting Default: 15");
			this.speedPerSpawn = 15;
		}
		
		if(burst) // if "true", release all the particles instantly (in a burst)
		{
			for(int i = 0; i < numberToSpawn; i++)
				level.addEntity(new Particle(x, y, level, color, size, life, varyParticleLifeSpan));
			
			setRemoved(true);
		}
	}


	public void tick()
	{	
		time++;
		
		if(time >= speedPerSpawn && !isRemoved())
		{
			time = 0;
			level.addEntity(new Particle(x, y, level, color, size, life, varyParticleLifeSpan));
			numberToSpawn--;
			
			if(numberToSpawn <= 0)
				setRemoved(true);
		}
	}


	public void render() 
	{ 
		// Spawners are invisible and there is no need to render anything 
	}
}
