package com.aer.Entity;

import com.aer.Levels.Level;

public abstract class Entity 
{
	public double x, y;
	protected Level level;
	protected boolean removed = false;
	
	public Entity(double x, double y, Level level)
	{
		this.x = x;
		this.y = y;
		this.level = level;
	}
	
	public abstract void tick();
	public abstract void render();
	
	public void setRemoved(boolean r)
	{
		removed = r;
	}
	
	public boolean isRemoved()
	{
		return removed;
	}
	
	public void setCoordinates(double x, double y)
	{
		this.x = x;
		this.y = y;
	}
}
